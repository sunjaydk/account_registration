'use strict';

var webpackDistConfig = require('./webpack.dist.config.js'),
    webpackDevConfig = require('./webpack.config.js');

module.exports = function (grunt) {
    require('time-grunt')(grunt);
    // Let *load-grunt-tasks* require everything
    require('load-grunt-tasks')(grunt);


    // Read configuration from package.json
    var pkgConfig = grunt.file.readJSON('package.json');

    grunt.initConfig({
        pkg: pkgConfig,

        auto_install: {
            local: {}
        },

        webpack: {
            options: {
                stats: !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
            },
            prod: webpackDistConfig,
            dev: Object.assign({ watch: true }, webpackDistConfig)
        },

        'webpack-dev-server': {
            options: {
                hot: true,
                host: '0.0.0.0',
                port: 3000,
                webpack: webpackDevConfig,
                publicPath: '/assets/',
                contentBase: './<%= pkg.src %>/',
                disableHostCheck: true
            },

            keepalive: true
        },

        copy: {
            dist: {
                files: [
                    // includes files within path
                    {
                        flatten: true,
                        expand: true,
                        src: ['<%= pkg.src %>/*'],
                        dest: '<%= pkg.dist %>/',
                        filter: 'isFile'
                    },
                    {
                        flatten: true,
                        expand: true,
                        src: ['<%= pkg.src %>/images/*'],
                        dest: '<%= pkg.dist %>/images/'
                    }
                ]
            }
        },

        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '<%= pkg.dist %>'
                    ]
                }]
            }
        },

        run: {
            options: {
                failOnError: true
            },
            test: {
                exec: 'jest'
            }
        }
    });

    grunt.registerTask('serve', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'open:dist', 'connect:dist']);
        }

        grunt.task.run([
            'webpack-dev-server'
        ]);
    });

    grunt.registerTask('build', ['auto_install', 'clean', 'copy', 'run:test', 'webpack']);
    grunt.registerTask('test', ['clean', 'run:test']);
    grunt.registerTask('default', ['auto_install', 'clean', 'copy', 'run:test', 'webpack']);
    grunt.registerTask('pre-prod', ['clean', 'copy', 'run:test']);

};


