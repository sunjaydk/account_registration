'use strict';

var path = require('path');
var webpack = require('webpack');
var HappyPack = require('happypack');

module.exports = {

    output: {
        filename: 'main.js',
        path: path.join(__dirname, 'dist/assets/'),
        publicPath: '/dist/assets/'
    },

    devtool: 'cheap-source-map',
    entry: [
        'webpack/hot/only-dev-server',
        'babel-polyfill',
        './src/main.js'
    ],
    cache: true,

    target: "web",

    stats: {
        colors: true,
        reasons: true
    },

    resolve: {
        extensions: ['.js', '.jsx', '.json'],
        alias: {
            'mixins': __dirname + '/src/mixins/',
            'components': __dirname + '/src/components/',
            'stores': __dirname + '/src/stores/',
            'actions': __dirname + '/src/actions/',
            'services': __dirname + '/src/services/',
            'const': __dirname + '/src/const/',
            'models': __dirname + '/src/models/',
            'utils': __dirname + '/src/utils/',
            'pages': __dirname + '/src/components/pages/'
        },
        unsafeCache: true
    },
    module: {
        rules: [
            {test: /\.(js|jsx)$/, exclude: /(node_modules|bower_components)/, loader: 'happypack/loader?id=eslint', enforce: "pre"},
            {test: /\.(js|jsx)$/, exclude: /(node_modules|bower_components)/, loader: 'happypack/loader?id=babel'},
            {test: /\.(png|jpg|jpeg|gif)$/, loader: 'url-loader?limit=8192'},
            {test: /\.json$/, loader: 'json-loader'},
            {test: /\.(css|less)$/, loader: 'style-loader!css-loader!less-loader'},

            {test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: "url-loader?limit=10000&minetype=application/font-woff"},
            {test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: "url-loader?limit=10000&minetype=application/font-woff"},
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url-loader?limit=10000&minetype=application/octet-stream"},
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader"},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url-loader?limit=10000&minetype=image/svg+xml"}
        ]
    },

    plugins: [
        new HappyPack({
            id: "babel",
            loaders: ['babel-loader?cacheDirectory=true'],
            threads: 4
        }),
        new HappyPack({
            id: "eslint",
            loaders: ['eslint-loader'],
            threads: 4
        }),
        new webpack.DefinePlugin({
            'process.env':{
                'NODE_ENV': JSON.stringify('development')
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: true,
            options: {
                context: __dirname
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
            $:      "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        })
    ]

};
