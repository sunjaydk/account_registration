/**
 * Created by Sunjay on February 18 2021
 */

'use strict';

import React from 'react';
import { render, fireEvent, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import ProgressBar from 'components/pages/account_registration/ProgressBar';
import AccountRegistrationScreens from 'const/AccountRegistrationScreens'
import Events from 'const/Events';
import AppDispatcher from 'dispatcher/AppDispatcher';

afterEach(cleanup);

describe('ProgressBar', () => {
    let currentScreen;
    const goToScreen = (screen) => {currentScreen = screen};

    beforeEach(() => {
        goToScreen(null);
    });

    it('displays breadcrumbs without id feed enabled', () => {
        AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, inmateIdName: 'Facility ID', useIDFeed: false});
        let dom = render(<ProgressBar currentScreen={AccountRegistrationScreens.INMATE_ID} goToScreen={goToScreen}/>);
        expect(dom.queryByText('Name')).toBeInTheDocument();
        expect(dom.queryByText('Facility ID')).toBeInTheDocument();
        expect(dom.queryByText('Date of Birth')).toBeInTheDocument();
        expect(dom.queryByText('Password')).toBeInTheDocument();
        expect(dom.queryByText('Security Questions')).toBeInTheDocument();
        expect(dom.queryByText('Review')).toBeInTheDocument();

    });

    it('displays breadcrumbs without name if id feed is enabled', () => {
        AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, inmateIdName: 'Facility ID', useIDFeed: true});
        let dom = render(<ProgressBar currentScreen={AccountRegistrationScreens.INMATE_ID} goToScreen={goToScreen}/>);
        expect(dom.queryByText('Name')).not.toBeInTheDocument();
        expect(dom.queryByText('Facility ID')).toBeInTheDocument();
        expect(dom.queryByText('Date of Birth')).toBeInTheDocument();
        expect(dom.queryByText('Password')).toBeInTheDocument();
        expect(dom.queryByText('Security Questions')).toBeInTheDocument();
        expect(dom.queryByText('Review')).toBeInTheDocument();
    });

    it('redirects you if you click a breadcrumbs for a completed section', () => {
        AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, inmateIdName: 'Facility ID', useIDFeed: false});
        AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value: 'Luke', inputName: 'firstName'});
        AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value: 'Sunborn', inputName: 'lastName'});

        let dom = render(<ProgressBar currentScreen={AccountRegistrationScreens.INMATE_ID} goToScreen={goToScreen}/>);
        fireEvent.click(dom.queryByText('Name'));
        expect(currentScreen).toEqual(AccountRegistrationScreens.NAME);
    });

    it('does not work for breadcrumbs that have not been filled out', () => {
        AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, inmateIdName: 'Facility ID', useIDFeed: false});
        AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value: '', inputName: 'firstName'});

        let dom = render(<ProgressBar currentScreen={AccountRegistrationScreens.NAME} goToScreen={goToScreen}/>);
        fireEvent.click(dom.queryByText('Facility ID'));
        expect(currentScreen).toEqual(null);
    });
});
