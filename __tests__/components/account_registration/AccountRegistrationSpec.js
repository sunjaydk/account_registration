/**
 * Created by Sunjay on February 18 2021
 */

'use strict';

import React from 'react';
import {render, cleanup, fireEvent} from '@testing-library/react';
import "@testing-library/jest-dom/extend-expect";
import AccountRegistration from '../../../src/components/pages/account_registration/AccountRegistration';
import Events from '../../../src/const/Events';
import AppDispatcher from '../../../src/dispatcher/AppDispatcher';
import AccountRegistrationScreens from 'const/AccountRegistrationScreens';

describe('AccountRegistration', () => {

    afterEach(cleanup);

    it('displays the name screen if id feed is disabled', () => {
        let dom = render(<AccountRegistration/>);
        expect(dom.queryByText('Loading...')).toBeInTheDocument();
        AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, inmateIdName: 'Facility ID', useIDFeed: false});
        expect(dom.queryByText('Loading...')).not.toBeInTheDocument();
        expect(dom.getByPlaceholderText('Legal First Name')).toBeInTheDocument();
        expect(dom.getByPlaceholderText('Legal Last Name')).toBeInTheDocument();
    });

    it('displays the inmate ID screen if id feed is enabled', () => {
        let dom = render(<AccountRegistration/>);
        expect(dom.queryByText('Loading...')).toBeInTheDocument();
        AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, inmateIdName: 'Facility ID', useIDFeed: true});
        expect(dom.queryByText('Loading...')).not.toBeInTheDocument();
        expect(dom.getByPlaceholderText('Facility ID')).toBeInTheDocument();
    });

    it('redirects you from the back button', () => {
        AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value: 'Carly Rae', inputName: 'firstName'});
        AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value: 'Jepsen', inputName: 'lastName'});
        AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value: '202020', inputName: 'inmateId'});
        AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value: '1985-11-21', inputName: 'dateOfBirth'});

        let dom = render(<AccountRegistration/>);

        AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, useIDFeed: false, inmateIdName: 'Facility ID'});

        fireEvent.click(dom.getByTestId(AccountRegistrationScreens.PASSWORD));
        fireEvent.click(dom.getByText('Back'));

        expect(dom.getByLabelText('Date of Birth')).toBeInTheDocument();

        fireEvent.click(dom.getByText('Back'));

        expect(dom.getByPlaceholderText('Facility ID')).toBeInTheDocument();

        fireEvent.click(dom.getByText('Back'));

        expect(dom.getByPlaceholderText('Legal First Name')).toBeInTheDocument();
    });
});
