/**
 * Created by Sunjay on March 24 2021
 */

'use strict';

import React from 'react';
import { cleanup, fireEvent, render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import AccountRegistrationScreens from '../../../src/const/AccountRegistrationScreens';
import ReviewScreen from '../../../src/components/pages/account_registration/screens/ReviewScreen';
import AppDispatcher from '../../../src/dispatcher/AppDispatcher';
import Events from '../../../src/const/Events';

const registrationInfo = {
    firstName: 'Elliot',
    lastName: 'Schafer',
    inmateId: 'elliot',
    dateOfBirth: '1995-06-28',
    password: 'p@ssw0rd',
    securityQuestion1: '1',
    securityAnswer1: 'Peace in The Borderlands',
    securityQuestion2: '2',
    securityAnswer2: 'I didn\'t have any',
};

const securityQuestions = [
    {question: 'What is your quest?', id: '1'},
    {question: 'Who is your childhood best friend?', id: '2'}
];

describe('ReviewScreen', () => {
    let dom;
    let currentScreen;
    const goToScreen = (screen) => {currentScreen = screen};

    beforeEach(() => {
        for (let key in registrationInfo) {
            AppDispatcher.dispatch({
                type: Events.UPDATE_REGISTRATION_INFO,
                value: registrationInfo[key],
                inputName: key,
            });
        }
        dom = render(<ReviewScreen goToScreen={goToScreen}/>);
        AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, inmateIdName: 'Facility ID', useIDFeed: false});
        AppDispatcher.dispatch({type: Events.SECURITY_QUESTIONS_LOADED, securityQuestions});
    });

    afterEach(cleanup);

    it('shows all filled in fields', () => {
        expect(dom.getByText('Elliot')).toBeInTheDocument();
        expect(dom.getByText('Schafer')).toBeInTheDocument();
        expect(dom.getByText('elliot')).toBeInTheDocument();
        expect(dom.getByText('1995-06-28')).toBeInTheDocument();
        expect(dom.getByTestId('password')).toBeInTheDocument();
        expect(dom.getByTestId('securityAnswer1')).toBeInTheDocument();
        expect(dom.getByTestId('securityAnswer2')).toBeInTheDocument();
        expect(dom.getByText('What is your quest?')).toBeInTheDocument();
        expect(dom.getByText('Who is your childhood best friend?')).toBeInTheDocument();
    });

    it('shows hidden fields', () => {
        expect(dom.queryByText('p@ssw0rd')).not.toBeInTheDocument();
        expect(dom.queryByText('Peace in The Borderlands')).not.toBeInTheDocument();
        expect(dom.queryByText('I didn\'t have any')).not.toBeInTheDocument();

        fireEvent.click(dom.getByText('Show hidden fields'));

        expect(dom.queryByText('p@ssw0rd')).toBeInTheDocument();
        expect(dom.queryByText('Peace in The Borderlands')).toBeInTheDocument();
        expect(dom.queryByText('I didn\'t have any')).toBeInTheDocument();
    });

    it('links to a previous page', () => {
        fireEvent.click(dom.getByText('Name'));
        expect(currentScreen).toEqual(AccountRegistrationScreens.NAME);
    });
});
