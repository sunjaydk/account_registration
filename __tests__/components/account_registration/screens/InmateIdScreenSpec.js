/**
 * Created by Sunjay on February 23 2021
 */

'use strict';
import React from 'react';
import { cleanup, fireEvent, render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import InmateIdScreen from '../../../src/components/pages/account_registration/screens/InmateIdScreen';
import AccountRegistrationScreens from '../../../src/const/AccountRegistrationScreens';
import Events from '../../../src/const/Events';
import AppDispatcher from '../../../src/dispatcher/AppDispatcher'

describe('InmateIdScreen', () => {
    afterEach(() => {
        cleanup();
        AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, inmateIdName: 'Facility ID'});
    });

    let currentScreen;
    const goToScreen = (screen) => {currentScreen = screen};

    it('requires an inmate id', () => {
        let dom = render(<InmateIdScreen goToScreen={goToScreen}/>);

        expect(dom.queryByText('Next')).toBeDisabled();
        expect(dom.queryAllByText('Facility ID is required.').length).toEqual(1);

        let inmateIdInput = dom.getByPlaceholderText('Facility ID');
        fireEvent.change(inmateIdInput, {target: {value: 'lsunborn1997'}});

        expect(inmateIdInput.value).toEqual('lsunborn1997');
        expect(dom.queryAllByText('Facility ID is required.').length).toEqual(0);
        expect(dom.queryByText('Next')).not.toBeDisabled();
    });

    it('uses custom inmate id name', () => {
        AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, inmateIdName: 'Learner ID'});
        let dom = render(<InmateIdScreen goToScreen={goToScreen}/>);

        expect(dom.queryAllByPlaceholderText('Facility ID').length).toEqual(0);
        expect(dom.queryAllByPlaceholderText('Learner ID').length).toEqual(1);

        fireEvent.change(dom.getByPlaceholderText('Learner ID'), {target: {value: ''}});

        expect(dom.queryAllByText('Facility ID is required.').length).toEqual(0);
        expect(dom.getByText('Learner ID is required.')).toBeInTheDocument();
    });

    it('displays an error and disables the next button if invalid characters are in the inmate id field', () => {
        let dom = render(<InmateIdScreen goToScreen={goToScreen}/>);
        let inmateIdInput = dom.getByPlaceholderText('Facility ID');

        fireEvent.change(inmateIdInput, {target: {value: 'l-sunborn1997'}});

        expect(dom.queryAllByText('Only letters and numbers are allowed.').length).toEqual(1);
        expect(dom.queryByText('Next')).toBeDisabled();
    });

    it('displays an error, disables the next button, and prevents typing if the inmate id is longer than fifty characters', () => {
        let dom = render(<InmateIdScreen goToScreen={goToScreen}/>);
        const longString = new Array(50).join('x');

        let inmateIdInput = dom.getByPlaceholderText('Facility ID');
        fireEvent.change(inmateIdInput, {target: {value: 'lsunborn1997' + longString}});

        expect(dom.queryByText('Facility ID cannot be longer than 50 characters.')).toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();
    });

    it('displays an error and disables the next button if the inmate id is invalid', () => {
        AppDispatcher.dispatch({type: Events.FAILED_REGISTRATION, invalidInmateId: true});

        let dom = render(<InmateIdScreen goToScreen={goToScreen}/>);

        expect(dom.queryByText('Sorry, but your Facility ID number may not match your facility\'s records assigned at booking or the information you provided at booking might require 24-48 hours to update.')).toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();

        let inmateIdInput = dom.getByPlaceholderText('Facility ID');
        fireEvent.change(inmateIdInput, {target: {value: 'lsunborn1997'}});

        expect(dom.queryByText('Sorry, but your Facility ID number may not match your facility\'s records assigned at booking or the information you provided at booking might require 24-48 hours to update.')).not.toBeInTheDocument();
        expect(dom.queryByText('Next')).not.toBeDisabled();
    });

    it('displays an error and disables the next button if the inmate id belongs to an account already', () => {
        AppDispatcher.dispatch({type: Events.FAILED_REGISTRATION, duplicateAccount: true});

        let dom = render(<InmateIdScreen goToScreen={goToScreen}/>);

        expect(dom.getByText('The Facility ID you provided already belongs to an account in our system.')).toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();

        let inmateIdInput = dom.getByPlaceholderText('Facility ID');
        fireEvent.change(inmateIdInput, {target: {value: 'dwavechaser1997'}});

        expect(dom.queryByText('The Facility ID you provided already belongs to an account in our system.')).not.toBeInTheDocument();
        expect(dom.queryByText('Next')).not.toBeDisabled();
    });

    it('clicking next loads the date of birth page', () => {
        let dom = render(<InmateIdScreen goToScreen={goToScreen}/>);
        let inmateIdInput = dom.getByPlaceholderText('Facility ID');
        fireEvent.change(inmateIdInput, {target: {value: 'lsunborn1997'}});

        expect(currentScreen).toBeUndefined();
        fireEvent.click(dom.getByText('Next'));
        expect(currentScreen).toEqual(AccountRegistrationScreens.DATE_OF_BIRTH);
    });
});
