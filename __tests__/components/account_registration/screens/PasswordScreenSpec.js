/**
 * Created by Sunjay on February 23 2021
 */

'use strict';

import React from 'react';
import { cleanup, fireEvent, render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import PasswordScreen from '../../../src/components/pages/account_registration/screens/PasswordScreen';
import AccountRegistrationScreens from '../../../src/const/AccountRegistrationScreens';
import Events from '../../../src/const/Events';
import PasswordValidators from '../../../src/const/PasswordValidators';
import AppDispatcher from '../../../src/dispatcher/AppDispatcher';

describe('PasswordScreen', () => {
    let dom;

    beforeEach(() => {
        dom = render(<PasswordScreen goToScreen={goToScreen}/>);
        AppDispatcher.dispatch({type: Events.PASSWORD_RULES_LOADED, rules: {}});
    })

    afterEach(cleanup);

    let currentScreen;
    const goToScreen = (screen) => {currentScreen = screen};

    it('requires matching passwords', () => {
        let passwordInput = dom.getByLabelText('Password');
        fireEvent.change(passwordInput, {target: {value: '1989 (Deluxe)'}});

        expect(passwordInput.value).toEqual('1989 (Deluxe)');
        expect(dom.queryAllByText('Passwords do not match.').length).toEqual(1);
        expect(dom.queryByText('Next')).toBeDisabled();

        fireEvent.change(dom.getByLabelText('Password confirmation'), {target: {value: '1989 (Deluxe)'}});

        expect(dom.queryAllByText('Passwords do not match.').length).toEqual(0);
        expect(dom.queryByText('Next')).not.toBeDisabled();
    });

    it('shows and validates on default password rules', () => {
        let passwordInput = dom.getByLabelText('Password');
        let passwordConfirmationInput = dom.getByLabelText('Password confirmation');

        fireEvent.change(passwordInput, {target: {value: '1989'}});
        fireEvent.change(passwordConfirmationInput, {target: {value: '1989'}});

        expect(dom.queryByText('Password must be at least 6 characters long')).toBeInTheDocument();
        expect(dom.queryByText('Password can be no more than 15 characters long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password must contain a letter')).toBeInTheDocument();
        expect(dom.queryByText('Password must contain a number')).not.toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();

        fireEvent.change(passwordInput, {target: {value: 'Red (Deluxe)'}});
        fireEvent.change(passwordConfirmationInput, {target: {value: 'Red (Deluxe)'}});

        expect(dom.queryByText('Password must be at least 6 characters long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password can be no more than 15 characters long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password must contain a letter')).not.toBeInTheDocument();
        expect(dom.queryByText('Password must contain a number')).toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();

        fireEvent.change(passwordInput, {target: {value: '1989 (Deluxe Edition)'}});
        fireEvent.change(passwordConfirmationInput, {target: {value: '1989 (Deluxe Edition)'}});

        expect(dom.queryByText('Password must be at least 6 characters long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password can be no more than 15 characters long')).toBeInTheDocument();
        expect(dom.queryByText('Password must contain a letter')).not.toBeInTheDocument();
        expect(dom.queryByText('Password must contain a number')).not.toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();

        fireEvent.change(passwordInput, {target: {value: '1989 (Deluxe)'}});
        fireEvent.change(passwordConfirmationInput, {target: {value: '1989 (Deluxe)'}});

        expect(dom.queryByText('Password must be at least 6 characters long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password can be no more than 15 characters long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password must contain a letter')).not.toBeInTheDocument();
        expect(dom.queryByText('Password must contain a number')).not.toBeInTheDocument();
        expect(dom.queryByText('Next')).not.toBeDisabled();
    });

    //Individual custom password rules are already tested in /components/pages/my_edovo/my_profile_pages/PasswordChangeFormSpec.js
    it('shows and validates on custom password rules', () => {
        const rules = {
            [PasswordValidators.pMinlength]: 6,
            [PasswordValidators.pMinspecchars]: 1,
            [PasswordValidators.pMaxlength]: 15,
        }

        AppDispatcher.dispatch({type: Events.PASSWORD_RULES_LOADED, rules});
        let passwordInput = dom.getByLabelText('Password');
        let passwordConfirmationInput = dom.getByLabelText('Password confirmation');

        fireEvent.change(passwordInput, {target: {value: '1989'}});
        fireEvent.change(passwordConfirmationInput, {target: {value: '1989'}});

        expect(dom.queryByText('Password must be at least 6 character(s) long')).toBeInTheDocument();
        expect(dom.queryByText('Password can be no more than 15 character(s) long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password must have at least 1 special character(s)')).toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();

        fireEvent.change(passwordInput, {target: {value: '1989 Deluxe'}});
        fireEvent.change(passwordConfirmationInput, {target: {value: '1989 Deluxe'}});

        expect(dom.queryByText('Password must be at least 6 character(s) long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password can be no more than 15 character(s) long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password must have at least 1 special character(s)')).toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();

        fireEvent.change(passwordInput, {target: {value: '1989 (Deluxe Edition)'}});
        fireEvent.change(passwordConfirmationInput, {target: {value: '1989 (Deluxe Edition)'}});

        expect(dom.queryByText('Password must be at least 6 character(s) long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password can be no more than 15 character(s) long')).toBeInTheDocument();
        expect(dom.queryByText('Password must have at least 1 special character(s)')).not.toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();

        fireEvent.change(passwordInput, {target: {value: '1989 (Deluxe)'}});
        fireEvent.change(passwordConfirmationInput, {target: {value: '1989 (Deluxe)'}});

        expect(dom.queryByText('Password must be at least 6 character(s) long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password can be no more than 15 character(s) long')).not.toBeInTheDocument();
        expect(dom.queryByText('Password must have at least 1 special character(s)')).not.toBeInTheDocument();
        expect(dom.queryByText('Next')).not.toBeDisabled();
    });

    it('can show password', () => {
        fireEvent.change(dom.getByLabelText('Password'), {target: {value: '1989 (Deluxe)'}});
        fireEvent.change(dom.getByLabelText('Password confirmation'), {target: {value: '1989 (Deluxe)'}});

        expect(dom.getByLabelText('Password').type).toEqual('password');
        expect(dom.getByLabelText('Password confirmation').type).toEqual('password');

        fireEvent.click(dom.getByLabelText('Show password'));

        expect(dom.getByLabelText('Password').type).toEqual('text');
        expect(dom.getByLabelText('Password confirmation').type).toEqual('text');

        fireEvent.click(dom.getByLabelText('Show password'));

        expect(dom.getByLabelText('Password').type).toEqual('password');
        expect(dom.getByLabelText('Password confirmation').type).toEqual('password');

    });

    it('clicking next loads the security question page', () => {
        fireEvent.change(dom.getByLabelText('Password'), {target: {value: '1989 (Deluxe)'}});
        fireEvent.change(dom.getByLabelText('Password confirmation'), {target: {value: '1989 (Deluxe)'}});

        expect(currentScreen).toBeUndefined();
        fireEvent.click(dom.getByText('Next'));
        expect(currentScreen).toEqual(AccountRegistrationScreens.SECURITY_QUESTIONS);
    });
});
