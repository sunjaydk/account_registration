/**
 * Created by Sunjay on February 18 2021
 */

'use strict';

import React from 'react';
import { cleanup, fireEvent, render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import AccountRegistrationScreens from '../../../src/const/AccountRegistrationScreens';
import NameScreen from '../../../src/components/pages/account_registration/screens/NameScreen';

describe('NameScreen', () => {
    afterEach(cleanup);

    let currentScreen;
    const goToScreen = (screen) => {currentScreen = screen};

    it('requires both a first and last name', () => {
        let dom = render(<NameScreen goToScreen={goToScreen}/>);

        expect(dom.queryByText('Next')).toBeDisabled();
        expect(dom.queryAllByText('Name is required.').length).toEqual(2);

        let firstNameInput = dom.getByPlaceholderText('Legal First Name');
        fireEvent.change(firstNameInput, {target: {value: 'Luke'}});
        expect(firstNameInput.value).toEqual('Luke');
        expect(dom.queryByText('Next')).toBeDisabled();
        expect(dom.queryAllByText('Name is required.').length).toEqual(1);
        expect(dom.queryByText('Next')).toBeDisabled();

        fireEvent.change(firstNameInput, {target: {value: ''}});

        let lastNameInput = dom.getByPlaceholderText('Legal Last Name');
        fireEvent.change(lastNameInput, {target: {value: 'Sunborn'}});
        expect(lastNameInput.value).toEqual('Sunborn');
        expect(dom.queryAllByText('Name is required.').length).toEqual(1);
        expect(dom.queryByText('Next')).toBeDisabled();

        fireEvent.change(firstNameInput, {target: {value: 'Luke'}});
        expect(dom.queryAllByText('Name is required.').length).toEqual(0);
        expect(dom.queryByText('Next')).not.toBeDisabled();
    });

    it('displays an error and disables the next button if invalid characters are in the first name field', () => {
        let dom = render(<NameScreen goToScreen={goToScreen}/>);
        let firstNameInput = dom.getByPlaceholderText('Legal First Name');

        fireEvent.change(firstNameInput, {target: {value: 'Serene'}});
        fireEvent.change(dom.getByPlaceholderText('Legal Last Name'), {target: {value: 'Heart-In-The-Chaos-Of-Battle'}});

        expect(dom.queryByText('Only letters, spaces, apostrophes (\') and hyphens (-) are allowed.')).toEqual(null)
        expect(dom.queryByText('Next')).not.toBeDisabled();

        fireEvent.change(firstNameInput, {target: {value: 'Serene*'}});

        expect(dom.queryByText('Only letters, spaces, apostrophes (\') and hyphens (-) are allowed.')).toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();
    });

    it('displays an error and disables the next button if invalid characters are in the last name field', () => {
        let dom = render(<NameScreen goToScreen={goToScreen}/>);

        let lastNameInput = dom.getByPlaceholderText('Legal Last Name');

        fireEvent.change(dom.getByPlaceholderText('Legal First Name'), {target: {value: 'Elliot'}});
        fireEvent.change(lastNameInput, {target: {value: 'Schafer'}});

        expect(dom.queryByText('Only letters, spaces, apostrophes (\') and hyphens (-) are allowed.')).toEqual(null)
        expect(dom.queryByText('Next')).not.toBeDisabled();

        fireEvent.change(lastNameInput, {target: {value: 'Schafer%'}});

        expect(dom.queryByText('Only letters, spaces, apostrophes (\') and hyphens (-) are allowed.')).toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();
    });

    it('displays an error, disables the next button, and prevents typing if the name is longer than fifty characters', () => {
        let dom = render(<NameScreen goToScreen={goToScreen}/>);

        let lastNameInput = dom.getByPlaceholderText('Legal Last Name');

        fireEvent.change(dom.getByPlaceholderText('Legal First Name'), {target: {value: 'Golden'}});
        fireEvent.change(lastNameInput, {target: {value: 'Hair-Scented-Like-Summer'}});

        expect(dom.queryByText('Name cannot be longer than 50 characters.')).toEqual(null)
        expect(dom.queryByText('Next')).not.toBeDisabled();

        fireEvent.change(lastNameInput, {target: {value: 'Hair-Scented-Like-Summer-Heart-In-The-Chaos-Of-Battle'}});

        expect(dom.queryByText('Name cannot be longer than 50 characters.')).toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();
    });

    it('clicking next loads the inmate id screen', () => {
        let dom = render(<NameScreen goToScreen={goToScreen}/>);
        let firstNameInput = dom.getByPlaceholderText('Legal First Name');
        fireEvent.change(firstNameInput, {target: {value: 'Luke'}});
        let lastNameInput = dom.getByPlaceholderText('Legal Last Name');
        fireEvent.change(lastNameInput, {target: {value: 'Sunborn'}});

        expect(currentScreen).toBeUndefined();
        fireEvent.click(dom.getByText('Next'));
        expect(currentScreen).toEqual(AccountRegistrationScreens.INMATE_ID);
    });
});
