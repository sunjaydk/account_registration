/**
 * Created by Sunjay on February 25 2021
 */

'use strict';

import React from 'react';
import { cleanup, fireEvent, render, within } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import AccountRegistrationScreens from '../../../src/const/AccountRegistrationScreens';
import SecurityQuestionScreen from '../../../src/components/pages/account_registration/screens/SecurityQuestionScreen';
import Events from '../../../src/const/Events';
import AppDispatcher from '../../../src/dispatcher/AppDispatcher';

describe('SecurityQuestionScreen', () => {
    let dom;
    let currentScreen;
    const goToScreen = (screen) => {currentScreen = screen};

    beforeEach(() => {
        dom = render(<SecurityQuestionScreen goToScreen={goToScreen}/>);
        AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value: 0, inputName: 'securityQuestion1'});
        AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value: 0, inputName: 'securityQuestion2'});
        AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value: '', inputName: 'securityAnswer1'});
        AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value: '', inputName: 'securityAnswer2'});
        AppDispatcher.dispatch({type: Events.SECURITY_QUESTIONS_LOADED, securityQuestions: [
            {question: 'What is your quest?', id: 1},
            {question: 'Who is your childhood best friend?', id: 2}
        ]});
    });

    afterEach(cleanup);

    it('the same question cannot be selected if it was already selected', () => {
        let securityQuestion1Selector = dom.getByPlaceholderText('Select first security question...');
        let securityQuestion2Selector = dom.getByPlaceholderText('Select second security question...');

        expect(within(securityQuestion1Selector).queryByText('What is your quest?')).not.toBeNull();
        expect(within(securityQuestion1Selector).queryByText('Who is your childhood best friend?')).not.toBeNull()
        expect(within(securityQuestion2Selector).queryByText('What is your quest?')).not.toBeNull();
        expect(within(securityQuestion2Selector).queryByText('Who is your childhood best friend?')).not.toBeNull()

        fireEvent.change(securityQuestion1Selector, {target: {value: 1}});

        expect(within(securityQuestion1Selector).queryByText('What is your quest?')).not.toBeNull();
        expect(within(securityQuestion1Selector).queryByText('Who is your childhood best friend?')).not.toBeNull()
        expect(within(securityQuestion2Selector).queryByText('What is your quest?')).toBeNull();
        expect(within(securityQuestion2Selector).queryByText('Who is your childhood best friend?')).not.toBeNull()

        fireEvent.change(securityQuestion2Selector, {target: {value: 2}});

        expect(within(securityQuestion1Selector).queryByText('What is your quest?')).not.toBeNull();
        expect(within(securityQuestion1Selector).queryByText('Who is your childhood best friend?')).toBeNull()
        expect(within(securityQuestion2Selector).queryByText('What is your quest?')).toBeNull();
        expect(within(securityQuestion2Selector).queryByText('Who is your childhood best friend?')).not.toBeNull()
    });

    it('all questions and answers are required', () => {
        expect(dom.getByText('Next')).toBeDisabled();
        expect(dom.queryByText('First security answer is required.')).not.toBeNull();
        expect(dom.queryByText('Second security answer is required.')).not.toBeNull();

        let securityQuestion1Selector = dom.getByPlaceholderText('Select first security question...');
        let securityQuestion2Selector = dom.getByPlaceholderText('Select second security question...');
        fireEvent.change(securityQuestion1Selector, {target: {value: 1}});
        fireEvent.change(securityQuestion2Selector, {target: {value: 2}});

        expect(dom.getByText('Next')).toBeDisabled();
        expect(dom.queryByText('First security answer is required.')).not.toBeNull();
        expect(dom.queryByText('Second security answer is required.')).not.toBeNull();

        let securityAnswer1Input = dom.getByPlaceholderText('First Security Answer');
        fireEvent.change(securityAnswer1Input, {target: {value: 'Peace in The Borderlands'}});

        expect(securityAnswer1Input.value).toEqual('Peace in The Borderlands');
        expect(dom.getByText('Next')).toBeDisabled();
        expect(dom.queryByText('First security answer is required.')).toBeNull();
        expect(dom.queryByText('Second security answer is required.')).not.toBeNull();

        fireEvent.change(securityAnswer1Input, {target: {value: ''}});

        let securityAnswer2Input = dom.getByPlaceholderText('Second Security Answer');
        fireEvent.change(securityAnswer2Input, {target: {value: 'I didn\'t have any'}});

        expect(securityAnswer2Input.value).toEqual('I didn\'t have any');
        expect(dom.getByText('Next')).toBeDisabled();
        expect(dom.queryByText('First security answer is required.')).not.toBeNull();
        expect(dom.queryByText('Second security answer is required.')).toBeNull();

        fireEvent.change(securityAnswer1Input, {target: {value: 'Peace in The Borderlands'}});

        expect(dom.queryByText('First security answer is required.')).toBeNull();
        expect(dom.queryByText('Second security answer is required.')).toBeNull();
        expect(dom.getByText('Next')).not.toBeDisabled();
    });

    it('the security answers cannot be the same', () => {
        let securityQuestion1Selector = dom.getByPlaceholderText('Select first security question...');
        let securityQuestion2Selector = dom.getByPlaceholderText('Select second security question...');
        let securityAnswer1Input = dom.getByPlaceholderText('First Security Answer');
        let securityAnswer2Input = dom.getByPlaceholderText('Second Security Answer');

        fireEvent.change(securityQuestion1Selector, {target: {value: 1}});
        fireEvent.change(securityQuestion2Selector, {target: {value: 2}});
        fireEvent.change(securityAnswer1Input, {target: {value: 'Peace in The Borderlands'}});

        expect(dom.queryByText('Security answers must not be the same.')).toBeNull();

        fireEvent.change(securityAnswer2Input, {target: {value: 'Peace in The Borderlands'}});

        expect(dom.queryByText('Security answers must not be the same.')).not.toBeNull();
        expect(dom.getByText('Next')).toBeDisabled();
    });

    it('displays an error, disables the next button, and prevents typing if the name is longer than fifty characters', () => {
        let securityQuestion1Selector = dom.getByPlaceholderText('Select first security question...');
        let securityQuestion2Selector = dom.getByPlaceholderText('Select second security question...');
        let securityAnswer1Input = dom.getByPlaceholderText('First Security Answer');
        let securityAnswer2Input = dom.getByPlaceholderText('Second Security Answer');

        fireEvent.change(securityQuestion1Selector, {target: {value: 1}});
        fireEvent.change(securityQuestion2Selector, {target: {value: 2}});
        fireEvent.change(securityAnswer2Input, {target: {value: 'I didn\'t have any'}});

        expect(dom.queryByText('First security answer cannot be longer than 50 characters.')).toBeNull();
        expect(dom.queryByText('Second security answer cannot be longer than 50 characters.')).toBeNull();

        fireEvent.change(securityAnswer1Input, {target: {value: 'Peace, justice, and diplomatic security in The Borderlands'}});

        expect(dom.queryByText('First security answer cannot be longer than 50 characters.')).not.toBeNull();
        expect(dom.queryByText('Second security answer cannot be longer than 50 characters.')).toBeNull();
        expect(dom.getByText('Next')).toBeDisabled();

        fireEvent.change(securityAnswer1Input, {target: {value: 'Peace in The Borderlands'}});
        fireEvent.change(securityAnswer2Input, {target: {value: 'I didn\'t have any and I resent the question! Kids are terrible'}});

        expect(dom.queryByText('First security answer cannot be longer than 50 characters.')).toBeNull();
        expect(dom.queryByText('Second security answer cannot be longer than 50 characters.')).not.toBeNull();
        expect(dom.getByText('Next')).toBeDisabled();
    });

    it('displays an error and disables the next button if the security questions are invalid', () => {
        AppDispatcher.dispatch({type: Events.FAILED_REGISTRATION, invalidSecurityQuestion: true});

        let dom = render(<SecurityQuestionScreen goToScreen={goToScreen}/>);

        expect(dom.queryByText('Invalid security questions. Please select other questions.')).toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();

        let securityQuestion1Selector = dom.getByPlaceholderText('Select first security question...');
        let securityQuestion2Selector = dom.getByPlaceholderText('Select second security question...');
        let securityAnswer1Input = dom.getByPlaceholderText('First Security Answer');
        let securityAnswer2Input = dom.getByPlaceholderText('Second Security Answer');

        fireEvent.change(securityQuestion1Selector, {target: {value: 1}});
        fireEvent.change(securityQuestion2Selector, {target: {value: 2}});
        fireEvent.change(securityAnswer1Input, {target: {value: 'Peace in The Borderlands'}});
        fireEvent.change(securityAnswer2Input, {target: {value: 'I didn\'t have any'}});

        expect(dom.queryByText('Invalid security questions. Please select other questions.')).not.toBeInTheDocument();
        expect(dom.queryByText('Next')).not.toBeDisabled();
    });

    it('clicking next loads the review screen', () => {
        let securityQuestion1Selector = dom.getByPlaceholderText('Select first security question...');
        let securityQuestion2Selector = dom.getByPlaceholderText('Select second security question...');
        let securityAnswer1Input = dom.getByPlaceholderText('First Security Answer');
        let securityAnswer2Input = dom.getByPlaceholderText('Second Security Answer');

        fireEvent.change(securityQuestion1Selector, {target: {value: 1}});
        fireEvent.change(securityQuestion2Selector, {target: {value: 2}});
        fireEvent.change(securityAnswer1Input, {target: {value: 'Peace in The Borderlands'}});
        fireEvent.change(securityAnswer2Input, {target: {value: 'I didn\'t have any'}});

        expect(currentScreen).toBeUndefined();
        fireEvent.click(dom.getByText('Next'));
        expect(currentScreen).toEqual(AccountRegistrationScreens.REVIEW);
    });
});
