/**
 * Created by Sunjay on February 24 2021
 */

'use strict';

import React from 'react';
import { cleanup, fireEvent, render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import DateOfBirthScreen from '../../../src/components/pages/account_registration/screens/DateOfBirthScreen';
import AccountRegistrationScreens from '../../../src/const/AccountRegistrationScreens';
import Events from '../../../src/const/Events';
import AppDispatcher from '../../../src/dispatcher/AppDispatcher';

describe('DateOfBirthScreen', () => {
    afterEach(cleanup);

    let currentScreen;
    const goToScreen = (screen) => {currentScreen = screen};

    it('requires a date of birth', () => {
        let dom = render(<DateOfBirthScreen goToScreen={goToScreen}/>);

        expect(dom.queryByText('Next')).toBeDisabled();

        let dobInput = dom.getByLabelText('Date of Birth');
        fireEvent.change(dobInput, {target: {value: '1985-11-21'}});

        expect(dobInput.value).toEqual('1985-11-21');
        expect(dom.queryByText('Next')).not.toBeDisabled();
    });

    it('displays an error and disables the next button if the birth date is invalid', () => {
        AppDispatcher.dispatch({type: Events.FAILED_REGISTRATION, invalidDOB: true});

        let dom = render(<DateOfBirthScreen goToScreen={goToScreen}/>);

        expect(dom.queryByText('The date of birth you gave does not match our records. Please confirm your date of birth is entered correctly.')).toBeInTheDocument();
        expect(dom.queryByText('Next')).toBeDisabled();

        let dobInput = dom.getByLabelText('Date of Birth');
        fireEvent.change(dobInput, {target: {value: '1985-06-21'}});

        expect(dom.queryByText('The date of birth you gave does not match our records. Please confirm your date of birth is entered correctly.')).not.toBeInTheDocument();
        expect(dom.queryByText('Next')).not.toBeDisabled();
    });

    it('clicking next loads the password page', () => {
        let dom = render(<DateOfBirthScreen goToScreen={goToScreen}/>);
        let dobInput = dom.getByLabelText('Date of Birth');
        fireEvent.change(dobInput, {target: {value: '1989-12-13'}});

        expect(currentScreen).toBeUndefined();
        fireEvent.click(dom.getByText('Next'));
        expect(currentScreen).toEqual(AccountRegistrationScreens.PASSWORD);
    });
});
