'use strict';

import {EventEmitter} from 'events';
import Events from '../const/Events';

EventEmitter.prototype.setMaxListeners(100);

let EventMixin = Object.assign({}, EventEmitter.prototype, {
    emitChange() {
        this.emit(Events.CHANGE_EVENT);
    },

    addChangeListener(callback) {
        this.on(Events.CHANGE_EVENT, callback);
    },

    removeChangeListener(callback) {
        this.removeListener(Events.CHANGE_EVENT, callback);
    }
});

export default EventMixin;

