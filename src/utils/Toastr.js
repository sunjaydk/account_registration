'use strict';

import toastr from 'toastr';

const Toastr = {
    init() {
        toastr.options = {
            'closeButton': true,
            'debug': false,
            'newestOnTop': true,
            'progressBar': false,
            'positionClass': 'toast-bottom-center',
            'preventDuplicates': false,
            'showDuration': '300',
            'hideDuration': '1000',
            'timeOut': '7000',
            'extendedTimeOut': '1000',
            'showEasing': 'swing',
            'hideEasing': 'linear',
            'showMethod': 'fadeIn',
            'hideMethod': 'fadeOut'
        };
    },

    info(text, title) { // title is optional
        Toastr.remove();
        toastr.info(text, title);
    },

    warning(text, title) {
        Toastr.remove();
        toastr.warning(text, title);
    },

    success(text, title) {
        Toastr.remove();
        toastr.success(text, title);
    },

    error(text, title) {
        Toastr.remove();
        toastr.error(text, title);
    },

    remove() {
        toastr.remove();
    }
};

export default Toastr;
