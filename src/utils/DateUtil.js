/**
 * Created by Sunjay on February 24 2021
 */

'use strict';

export const getHTMLDateString = (dateInteger) => {
    let date = new Date(dateInteger);
    let year = (date.getFullYear()).toString();
    let month = (date.getMonth() + 1).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');
    return year + '-' + month + '-' + day;
};
