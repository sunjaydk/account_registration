'use strict';

import PasswordValidators from '../const/PasswordValidators';

const specialCharacterRegex = /([^a-zA-Z\d\s])/g;

const PasswordValidationUtil = {
    getDefaultPasswordValidationErrors(password) {
        let passwordErrors = [];
        if (password.length < 6) {
            passwordErrors.push('Password must be at least 6 characters long');
        } else if (password.length > 15) {
            passwordErrors.push('Password can be no more than 15 characters long');
        }
        if (!/[A-z]+/g.test(password)) {
            passwordErrors.push('Password must contain a letter');
        }
        if (!/\d+/g.test(password)) {
            passwordErrors.push('Password must contain a number');
        }
        return passwordErrors;
    },

    getPasswordValidationErrors(password, passwordRules) {
        let passwordErrors = [];
        Object.keys(passwordRules).forEach(key => {
            let matchArray;
            let value = passwordRules[key];
            switch (key) {
                case PasswordValidators.pMinlength:
                    if (password.length < parseInt(value)) {
                        passwordErrors.push('Password must be at least ' + value + ' character(s) long');
                    }
                    break;
                case PasswordValidators.pMaxlength:
                    if (password.length > parseInt(value)) {
                        passwordErrors.push('Password can be no more than ' + value + ' character(s) long');
                    }
                    break;
                case PasswordValidators.pMaxnumbers:
                    matchArray = password.match(/(\d)/g);
                    if (matchArray && matchArray.length > parseInt(value)) {
                        passwordErrors.push('Password can have no more than ' + value + ' number(s)');
                    }
                    break;
                case PasswordValidators.pMinnumbers:
                    matchArray = password.match(/(\d)/g);
                    if (!matchArray || matchArray.length < parseInt(value)) {
                        passwordErrors.push('Password must have at least ' + value + ' number(s)');
                    }
                    break;
                case PasswordValidators.pMinspecchars:
                    matchArray = password.match(specialCharacterRegex);
                    if (!matchArray || matchArray.length < parseInt(value)) {
                        passwordErrors.push('Password must have at least ' + value + ' special character(s)');
                    }
                    break;
                case PasswordValidators.pMaxspecchars:
                    matchArray = password.match(specialCharacterRegex);
                    if (matchArray && matchArray.length > parseInt(value)) {
                        passwordErrors.push('Password can have no more than ' + value + ' special character(s)');
                    }
                    break;
                case PasswordValidators.pMinletters:
                    matchArray = password.match(/([A-z])/g);
                    if (!matchArray || matchArray.length < parseInt(value)) {
                        passwordErrors.push('Password must have at least ' + value + ' letter(s)');
                    }
                    break;
                default:
            }
        });

        return passwordErrors;
    }
};

export default PasswordValidationUtil;
