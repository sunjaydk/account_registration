/**
 * Created by Sunjay on February 15 2021
 */

'use strict';

import StoreFactory from './StoreFactory';
import Events from '../const/Events';

let _firstName = '';
let _lastName = '';
let _inmateId = '';
let _dateOfBirth = undefined;
let _password = '';
let _securityQuestion1 = 0;
let _securityQuestion2 = 0;
let _securityAnswer1 = '';
let _securityAnswer2 = '';
let _registrationErrors = {};
let AccountRegistrationStore;
let handlers = {};

handlers[Events.UPDATE_REGISTRATION_INFO] = (action) => {
    AccountRegistrationStore[action.inputName] = action.value;

    if (action.inputName === 'dateOfBirth') {
        _registrationErrors.invalidDOB = false;
    } else if (action.inputName === 'inmateId') {
        _registrationErrors.invalidInmateId = false;
        _registrationErrors.duplicateAccount = false;
    } else if (action.inputName.includes('security')) {
        _registrationErrors.invalidSecurityQuestion = false;
    }
};

handlers[Events.FAILED_REGISTRATION] = ({invalidInmateId, duplicateAccount, invalidDOB, invalidSecurityQuestion}) => {
    _registrationErrors = {invalidInmateId, duplicateAccount, invalidDOB, invalidSecurityQuestion};
};

handlers[Events.RESET_PAGE] = () => {
    _firstName = '';
    _lastName = '';
    _inmateId = '';
    _dateOfBirth = undefined;
    _password = '';
    _securityQuestion1 = 0;
    _securityQuestion2 = 0;
    _securityAnswer1 = '';
    _securityAnswer2 = '';
    _registrationErrors = {};
};

AccountRegistrationStore = StoreFactory.createStore(handlers, {});

Object.defineProperties(AccountRegistrationStore, {
    firstName: {
        get: () => _firstName,
        set: (firstName) => {
            _firstName = firstName;
        },
    },

    lastName: {
        get: () => _lastName,
        set: (lastName) => {
            _lastName = lastName;
        },
    },

    inmateId: {
        get: () => _inmateId,
        set: (inmateId) => {
            _inmateId = inmateId;
        },
    },

    dateOfBirth: {
        get: () => _dateOfBirth,
        set: (dateOfBirth) => {
            _dateOfBirth = dateOfBirth;
        },
    },

    password: {
        get: () => _password,
        set: (password) => {
            _password = password;
        },
    },

    securityQuestion1: {
        get: () => _securityQuestion1,
        set: (securityQuestion1) => {
            _securityQuestion1 = securityQuestion1;
        },
    },

    securityQuestion2: {
        get: () => _securityQuestion2,
        set: (securityQuestion2) => {
            _securityQuestion2 = securityQuestion2;
        },
    },

    securityAnswer1: {
        get: () => _securityAnswer1,
        set: (securityAnswer1) => {
            _securityAnswer1 = securityAnswer1;
        },
    },

    securityAnswer2: {
        get: () => _securityAnswer2,
        set: (securityAnswer2) => {
            _securityAnswer2 = securityAnswer2;
        },
    },

    registrationErrors: {
        get: () => _registrationErrors,
    },

    invalidInmateId: {
        get: () => _registrationErrors.invalidInmateId,
    },

    invalidDOB: {
        get: () => _registrationErrors.invalidDOB,
    },

    invalidSecurityQuestion: {
        get: () => _registrationErrors.invalidSecurityQuestion,
    },

    duplicateAccount: {
        get: () => _registrationErrors.duplicateAccount,
    },
});

export default AccountRegistrationStore;
