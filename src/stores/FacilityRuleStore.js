/**
 * Created by Sunjay on February 22 2021
 */

'use strict';

import StoreFactory from './StoreFactory';
import Events from '../const/Events';

let _useIDFeed = false;
let _inmateIdName = 'Facility ID';
let FacilityRuleStore;
let handlers = {};

handlers[Events.LOAD_ID_FEED_RULES] = (action) => {
    _useIDFeed = action.useIDFeed;
    _inmateIdName = action.inmateIdName;
};

FacilityRuleStore = StoreFactory.createStore(handlers, {});

Object.defineProperties(FacilityRuleStore, {
    useIDFeed: {
        get: () => _useIDFeed,
    },

    inmateIdName: {
        get: () => _inmateIdName,
    },
});

export default FacilityRuleStore;
