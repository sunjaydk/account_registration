/**
 * Created by sunjaydk on February 25th 2021
 */

'use strict';

import StoreFactory from './StoreFactory';
import Events from '../const/Events';

let SecurityQuestionStore;

let _securityQuestions = [];

const handlers = {};

handlers[Events.SECURITY_QUESTIONS_LOADED] = (action) => {
    _securityQuestions = action.securityQuestions;
};

SecurityQuestionStore = StoreFactory.createStore(handlers);

Object.defineProperties(SecurityQuestionStore, {
    securityQuestions: {
        get: () => { return _securityQuestions || []; }
    },
});

export default SecurityQuestionStore;

