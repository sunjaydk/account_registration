'use strict';

import AppDispatcher from '../dispatcher/AppDispatcher';
import EventMixin from '../mixins/EventMixin';

let StoreFactory = {
    createStore(handlers, additionalParams) {
        if (arguments.length === 0) {
            throw new Error('You must provide at least \'handlers\' argument in order to create a new store');
        }
        if (additionalParams && typeof additionalParams !== 'object') {
            throw new Error('The second argument should be a valid \'key:value\' object');
        }
        let newStore = Object.assign({}, EventMixin, additionalParams);
        newStore.dispatchToken = AppDispatcher.register((action) => {
            if (handlers[action.type]) {
                handlers[action.type](action);
                newStore.emitChange();
            }
        });
        return newStore;
    }
};

export default StoreFactory;
