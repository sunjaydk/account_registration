/**
 * Created by Sunjay on January 15th 2021
 */

'use strict';

import StoreFactory from './StoreFactory';
import Events from '../const/Events';

let PasswordRuleStore;
let _passwordRules = {};
let handlers = {};
handlers[Events.PASSWORD_RULES_LOADED] = (action) => {
    _passwordRules = action.rules;
};

PasswordRuleStore = StoreFactory.createStore(handlers, {});

Object.defineProperties(PasswordRuleStore, {
    passwordRules: {
        get: () => { return _passwordRules; }
    },
});

export default PasswordRuleStore;
