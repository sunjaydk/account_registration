/**
 * Created by Sunjay on February 11 2021
 */

'use strict';

import keymirror from 'keymirror';

const AccountRegistrationScreens = keymirror({
    NAME: null,
    INMATE_ID: null,
    DATE_OF_BIRTH: null,
    PASSWORD: null,
    SECURITY_QUESTIONS: null,
    REVIEW: null,
});

export default AccountRegistrationScreens;
