'use strict';

const PasswordValidators = {
    pMinlength: 'p-minlength',
    pMaxlength: 'p-maxlength',
    pMaxnumbers: 'p-maxnumbers',
    pMinnumbers: 'p-minnumbers',
    pMinspecchars: 'p-minspecchars',
    pMaxspecchars: 'p-maxspecchars',
    pMinletters: 'p-minletters',
};

export default PasswordValidators;
