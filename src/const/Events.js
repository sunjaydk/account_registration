/**
 * Created by misha on 16-Oct-16.
 */

'use strict';

import keymirror from 'keymirror';

const Events = keymirror({
    UPDATE_REGISTRATION_INFO: null,
    LOAD_ID_FEED_RULES: null,
    SECURITY_QUESTIONS_LOADED: null,
    FAILED_REGISTRATION: null,
    RESET_PAGE: null,
});

export default Events;
