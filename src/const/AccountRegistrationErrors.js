import FacilityRuleStore from '../stores/FacilityRuleStore';

const AccountRegistrationErrors = {
    duplicateAccount: 'The ' + (FacilityRuleStore.inmateIdName || 'ID') + ' you provided already belongs to an account in our system.',
    invalidInmateId: 'Sorry, but your ' + (FacilityRuleStore.inmateIdName || 'ID') + ' number may not match your facility\'s ' +
        'records assigned at booking or the information you ' +
        'provided at booking might require 24-48 hours to update.',
    invalidDOB: 'The date of birth you gave does not match our records. ' +
        'Please confirm your date of birth is entered correctly.',
    invalidSecurityQuestion: 'Invalid security questions. Please select other questions.',
};

export default AccountRegistrationErrors;
