const Colors = {
    lighterBlue: '#d2efff',
    lightBlue: '#64c8ff',
    mediumBlue: '#309fd8',
    white: '#fff',
    mediumWhite: '#f8f8f8',
    blueWhite: '#cee9ff',
    green: '#3caf85',
    gray: '#d9d9d9',
    pink: '#ff8986',
    darkRed: '#880000',
    orange: '#ed813f',
    black: '#333333',
};

export default Colors;
