/**
 * Created by Sunjay on January 15th 2021
 */

'use strict';

import AppDispatcher from '../dispatcher/AppDispatcher';
import PasswordValidators from '../const/PasswordValidators';
import Events from '../const/Events';

// These rules are set by the facility and would normally be grabbed from the Java backend.
const rules = {
    [PasswordValidators.pMinlength]: 6,
    [PasswordValidators.pMinspecchars]: 1,
    [PasswordValidators.pMaxlength]: 15,
};

let PasswordRuleActions = {
    loadPasswordRules() {
        AppDispatcher.dispatch({type: Events.PASSWORD_RULES_LOADED, rules});
        // return Request.get(Urls.SERVICES.PASSWORD_RULES).then((response) => {
        //     let ruleArray = JSON.parse(response);
        //     let rules = {};
        //     if (ruleArray.length > 0) {
        //          rules = ruleArray.reduce((ruleHash, rule) => Object.assign({}, ruleHash, rule));
        //     }
        //     AppDispatcher.dispatch({type: Events.PASSWORD_RULES_LOADED, rules});
        // });
    },
};

export default PasswordRuleActions;
