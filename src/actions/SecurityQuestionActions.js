/**
 * Created by Sunjay on February 25 2021
 */

'use strict';

import Events from '../const/Events';
import AppDispatcher from '../dispatcher/AppDispatcher';

const SecurityQuestionActions = {
    loadSecurityQuestions() {
    	// These questions are also set on the facility level and we would normally grab them from the backend
        AppDispatcher.dispatch({type: Events.SECURITY_QUESTIONS_LOADED, securityQuestions: [
            {question: 'What is your quest?', id: 1},
            {question: 'Who is your childhood best friend?', id: 2}
        ]});
        // return Request.get('/account/security_questions').then((response) => {
        //     let {securityQuestions} = JSON.parse(response);
        //     AppDispatcher.dispatch({type: Events.SECURITY_QUESTIONS_LOADED, securityQuestions});
        // });
    },
};

export default SecurityQuestionActions;
