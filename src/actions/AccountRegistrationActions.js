/**
 * Created by Sunjay on February 15 2021
 */

'use strict';

import Events from '../const/Events';
import AppDispatcher from '../dispatcher/AppDispatcher';
import Toastr from '../utils/Toastr';

const AccountRegistrationActions = {
    init() {
        AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, useIDFeed: true, inmateIdName: 'Facility ID'});
        // return Request.get('/account/id_feed').then((response) => {
        //     let {useIDFeed, inmateIdName} = JSON.parse(response);
        //     AppDispatcher.dispatch({type: Events.LOAD_ID_FEED_RULES, useIDFeed, inmateIdName});
        // });
    },

    updateRegistration({value, inputName}) {
        return AppDispatcher.dispatch({type: Events.UPDATE_REGISTRATION_INFO, value, inputName});
    },

    register(fields) {
        Toastr.info('You\'d have successfully registered if this was actually hooked up to a back end!');
        console.log('Registration data: ' + fields.toString());        
        AppDispatcher.dispatch({type: Events.RESET_PAGE});      
        // return Request.post('/account/create', fields, true)
        //     .then((response) => {
        //         let data = JSON.parse(response);
        //         if (data.hasError) {
        //             let {invalidInmateId, invalidDOB, invalidSecurityQuestion, duplicateAccount} = data;
        //             AppDispatcher.dispatch({type: Events.FAILED_REGISTRATION, invalidInmateId, invalidDOB, invalidSecurityQuestion, duplicateAccount});
        //         } else {
        //             Toastr.info('You\'d have successfully registered if this was actually hooked up to a back end!')
        //             AppDispatcher.dispatch({type: Events.RESET_PAGE});
        //         }
        //     });
    }
};

export default AccountRegistrationActions;
