/**
 * Created by misha on 16-Oct-16.
 */

'use strict';

import React from 'react';
import Toastr from '../utils/Toastr';
import AccountRegistration from './account_registration/AccountRegistration';

class App extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        Toastr.init();
    }

    render() {
        return (
            <div className='container-fluid'>
                <AccountRegistration/>
            </div>
        );
    }
}

export default App;
