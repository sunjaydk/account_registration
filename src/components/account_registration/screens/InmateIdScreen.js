/**
 * Created by Sunjay on February 23 2021
 */

'use strict';

import PropTypes from 'prop-types';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import AccountRegistrationActions from '../../../actions/AccountRegistrationActions';
import AccountRegistrationScreens from '../../../const/AccountRegistrationScreens';
import AccountRegistrationStore from '../../../stores/AccountRegistrationStore';
import FacilityRuleStore from 'stores/FacilityRuleStore';
import RegistrationBox from '../RegistrationBox';
import RegistrationHeader from '../RegistrationHeader';
import RegistrationInput from '../RegistrationInput';
import AccountRegistrationErrors from '../../../const/AccountRegistrationErrors';

class InmateIdScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inmateId: AccountRegistrationStore.inmateId,
            inmateIdErrors: getErrors(AccountRegistrationStore.inmateId),
        };
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    }

    componentDidMount() {
        AccountRegistrationStore.addChangeListener(this._updateState);
    }

    componentWillUnmount() {
        AccountRegistrationStore.removeChangeListener(this._updateState);
    }

    _updateState = () => {
        let {inmateId} = AccountRegistrationStore;
        this.setState({inmateId, inmateIdErrors: getErrors(inmateId)});
    };

    _update = (value) => AccountRegistrationActions.updateRegistration({value, inputName: 'inmateId'});

    render() {
        return <div>
            <RegistrationHeader>
                <div className="text-center description-text">
                    Please enter your <strong>{FacilityRuleStore.inmateIdName}</strong> below:
                </div>
            </RegistrationHeader>
            <div className="row margin-top-40">
                <div className="col-xs-6 col-xs-offset-1">
                    {this._renderInput()}
                </div>
            </div>
        </div>;
    }

    _renderInput() {
        return <RegistrationBox>
            <RegistrationInput
                updateFunction={this._update}
                placeholder={FacilityRuleStore.inmateIdName}
                value={this.state.inmateId}
                name='inmate_id'
                errors={this.state.inmateIdErrors} />
            <div className="row">
                <button
                    disabled={this.state.inmateIdErrors.length > 0}
                    onClick={() => this.props.goToScreen(AccountRegistrationScreens.DATE_OF_BIRTH)}
                    className="next-btn col-md-8 col-sm-8 col-xs-8">Next</button>
            </div>
        </RegistrationBox>;
    }
}

InmateIdScreen.propTypes = {
    goToScreen: PropTypes.func.isRequired,
};

const getErrors = (inmateId) => {
    let inmateIdErrors = [];
    if (!inmateId || inmateId.length === 0) {
        inmateIdErrors.push(FacilityRuleStore.inmateIdName + ' is required.');
    } else if (inmateId.length > 50) {
        inmateIdErrors.push(FacilityRuleStore.inmateIdName + ' cannot be longer than 50 characters.');
    }
    if (/[^a-zA-Z\d]/.test(inmateId)) {
        inmateIdErrors.push('Only letters and numbers are allowed.');
    }
    if (AccountRegistrationStore.invalidInmateId) {
        inmateIdErrors.push(AccountRegistrationErrors.invalidInmateId);
    }
    if (AccountRegistrationStore.duplicateAccount) {
        inmateIdErrors.push(AccountRegistrationErrors.duplicateAccount);
    }
    return inmateIdErrors;
};

export default InmateIdScreen;
