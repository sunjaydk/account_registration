/**
 * Created by Sunjay on February 25 2021
 */

'use strict';
/** @jsx jsx */

import {css, jsx} from '@emotion/core';
import PropTypes from 'prop-types';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import AccountRegistrationActions from '../../../actions/AccountRegistrationActions';
import SecurityQuestionActions from '../../../actions/SecurityQuestionActions';
import AccountRegistrationErrors from '../../../const/AccountRegistrationErrors';
import AccountRegistrationScreens from '../../../const/AccountRegistrationScreens';
import Colors from '../../../const/Colors';
import AccountRegistrationStore from '../../../stores/AccountRegistrationStore';
import FacilityRuleStore from '../../../stores/FacilityRuleStore';
import SecurityQuestionStore from '../../../stores/SecurityQuestionStore';
import RegistrationHeader from '../RegistrationHeader';
import RegistrationBox from '../RegistrationBox';
import RegistrationError from '../RegistrationError';

class ReviewScreen extends React.Component {
    constructor(props) {
        super(props);

        let registrationFields = {
            firstName: AccountRegistrationStore.firstName,
            lastName: AccountRegistrationStore.lastName,
            inmateId: AccountRegistrationStore.inmateId,
            dateOfBirth: AccountRegistrationStore.dateOfBirth,
            password: AccountRegistrationStore.password,
            securityQuestion1: AccountRegistrationStore.securityQuestion1,
            securityAnswer1: AccountRegistrationStore.securityAnswer1,
            securityQuestion2: AccountRegistrationStore.securityQuestion2,
            securityAnswer2: AccountRegistrationStore.securityAnswer2,
        };

        this.state = {
            ...registrationFields,
            ...AccountRegistrationStore.registrationErrors,
            securityQuestions: SecurityQuestionStore.securityQuestions,
            isHidden: true,
            loading: true,
        };

        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    }

    componentDidMount() {
        SecurityQuestionStore.addChangeListener(this._populateSecurityQuestions);
        AccountRegistrationStore.addChangeListener(this._updateErrorState);
        if (this.state.securityQuestions.length === 0) {
            SecurityQuestionActions.loadSecurityQuestions();
        } else {
            this.setState({loading: false});
        }
    }

    componentWillUnmount() {
        AccountRegistrationStore.removeChangeListener(this._updateErrorState);
        SecurityQuestionStore.removeChangeListener(this._populateSecurityQuestions);
    }

    _updateErrorState = () => {
        this.setState({
            ...AccountRegistrationStore.registrationErrors,
        });
    }

    _populateSecurityQuestions = () => {
        this.setState({securityQuestions: SecurityQuestionStore.securityQuestions, loading: false});
    };

    render() {
        let nameOffset = FacilityRuleStore.useIDFeed ? 0 : 1;
        return <div>
            <RegistrationHeader>
                <div className='text-center description-text'>
                    <strong>Review your answers below: </strong>
                </div>;
            </RegistrationHeader>
            <div className='row margin-top-40'>
                <div className="col-xs-6 col-xs-offset-1">
                    <RegistrationBox>
                        {this._renderNameBlock()}
                        <LinkBlock index={nameOffset + 1} screenName={FacilityRuleStore.inmateIdName}
                                onClick={() => this.props.goToScreen(AccountRegistrationScreens.INMATE_ID)}>
                            <ReviewBlock
                                inputName='inmateId'
                                value={this.state.inmateId} />
                            {this._renderInmateIdError()}
                        </LinkBlock>
                        <LinkBlock index={nameOffset + 2} screenName='Date of Birth'
                                onClick={() => this.props.goToScreen(AccountRegistrationScreens.DATE_OF_BIRTH)}>
                            <ReviewBlock
                                inputName='dateOfBirth'
                                value={this.state.dateOfBirth} />
                            {this._renderDOBError()}
                        </LinkBlock>
                        <LinkBlock index={nameOffset + 3} screenName='Password'
                                onClick={() => this.props.goToScreen(AccountRegistrationScreens.PASSWORD)}>
                            <ReviewBlock
                                inputName='password'
                                value={this.state.password}
                                hide={this.state.isHidden} />
                        </LinkBlock>
                        <LinkBlock index={nameOffset + 4} screenName='Security Questions'
                                onClick={() => this.props.goToScreen(AccountRegistrationScreens.SECURITY_QUESTIONS)}>
                            <ReviewBlock
                                value={this._getSecurityQuestion(this.state.securityQuestion1)}
                                inputName='securityQuestion1' />
                            <ReviewBlock
                                inputName='securityAnswer1'
                                hide={this.state.isHidden}
                                value={this.state.securityAnswer1} />
                            <ReviewBlock
                                value={this._getSecurityQuestion(this.state.securityQuestion2)}
                                inputName='securityQuestion2' />
                            <ReviewBlock
                                inputName='securityAnswer2'
                                hide={this.state.isHidden}
                                value={this.state.securityAnswer2} />
                            {this._renderSecurityQuestionError()}
                        </LinkBlock>
                        <input type='checkbox'
                               id='show_hidden_fields'
                               name='show_hidden_fields'
                               checked={!this.state.isHidden}
                               onChange={() => this.setState({isHidden: !this.state.isHidden})}/>
                        <label css={css({marginLeft: '.4em'})} htmlFor='show_hidden_fields'>
                            Show hidden fields
                        </label>
                    </RegistrationBox>
                    <button className='btn btn-block btn-green-c btn-lg margin-bottom-60'
                            onClick={() => AccountRegistrationActions.register(this.state)}>
                        Submit Registration
                    </button>
                </div>
            </div>
        </div>;
    }

    _renderNameBlock() {
        if (FacilityRuleStore.useIDFeed) {
            return null;
        }
        return <LinkBlock index={1} screenName='Name'
                onClick={() => this.props.goToScreen(AccountRegistrationScreens.NAME)}>
            <ReviewBlock
                screen={AccountRegistrationScreens.NAME}
                inputName='firstName'
                value={this.state.firstName} />
            <ReviewBlock
                screen={AccountRegistrationScreens.NAME}
                inputName='lastName'
                value={this.state.lastName} />
        </LinkBlock>;
    }

    _getSecurityQuestion = (questionId) => {
        let question = this.state.securityQuestions.find(q => q.id.toString() === questionId);
        return question ? question.question : 'No Question Selected';
    };

    _renderInmateIdError() {
        if (this.state.invalidInmateId) {
            return <RegistrationError error={AccountRegistrationErrors.invalidInmateId} />;
        } else if (this.state.duplicateAccount) {
            return <RegistrationError error={AccountRegistrationErrors.duplicateAccount} />;
        }
        return null;
    }

    _renderDOBError() {
        if (this.state.invalidDOB) {
            return <RegistrationError error={AccountRegistrationErrors.invalidDOB} />;
        }
        return null;
    }

    _renderSecurityQuestionError() {
        if (this.state.invalidSecurityQuestion) {
            return <RegistrationError error={AccountRegistrationErrors.invalidSecurityQuestion} />;
        }
        return null;
    }
}

const LinkBlock = ({onClick, screenName, index, children}) => {
    return <div css={css(linkLineStyles)} onClick={onClick}>
        <span css={css(linkStyles)}>{index}</span>
        {screenName}
        <div>{children}</div>
    </div>;
};

const ReviewBlock = ({value, hide, inputName}) => {
    return <div css={css(reviewBlockStyles)}>
        <div data-testid={inputName} css={css(reviewInputStyles)}>
            {hide ? new Array(value.length).join('*') : value}
        </div>
    </div>;
};

const linkStyles = {
    width: '30px',
    height: '30px',
    borderRadius: '50%',
    textAlign: 'center',
    lineHeight: '25px',
    boxShadow: '0px 3px 15px rgba(0,0,0,0.2)',
    display: 'inline-block',
    position: 'absolute',
    top: '-4px',
    left: '-10px',
    border: '2px solid ' + Colors.green,
    backgroundColor: Colors.white,
    color: Colors.black,
};

const linkLineStyles = {
    position: 'relative',
    padding: '0px 0px 0px 29px',
    fontSize: 'medium',
    color: 'black',
    marginRight: '-10px',
    marginBottom: '10px',
};

const reviewBlockStyles = {
    position: 'relative',
    padding: '5px',
};

const reviewInputStyles = {
    height: '46px',
    padding: '10px 16px',
    fontSize: '18px',
    lineHeight: '1.33',
    borderRadius: '6px',
    backgroundColor: Colors.white,
    backgroundImage: 'none',
    border: '1px solid ' + Colors.gray,
    display: 'block',
    margin: '3px',
};

ReviewScreen.propTypes = {
    goToScreen: PropTypes.func.isRequired,
};

export default ReviewScreen;
