/**
 * Created by Sunjay on February 25 2021
 */

'use strict';
/** @jsx jsx */

import {css, jsx} from '@emotion/core';
import PropTypes from 'prop-types';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import AccountRegistrationActions from '../../../actions/AccountRegistrationActions';
import SecurityQuestionActions from '../../../actions/SecurityQuestionActions';
import AccountRegistrationErrors from '../../../const/AccountRegistrationErrors';
import AccountRegistrationScreens from '../../../const/AccountRegistrationScreens';
import Colors from '../../../const/Colors';
import AccountRegistrationStore from '../../../stores/AccountRegistrationStore';
import SecurityQuestionStore from '../../../stores/SecurityQuestionStore';
import RegistrationBox from '../RegistrationBox';
import RegistrationHeader from '../RegistrationHeader';
import RegistrationInput from '../RegistrationInput';

class SecurityQuestionScreen extends React.Component {
    constructor(props) {
        super(props);

        let questionsAndAnswers = {
            securityQuestion1: AccountRegistrationStore.securityQuestion1,
            securityAnswer1: AccountRegistrationStore.securityAnswer1,
            securityQuestion2: AccountRegistrationStore.securityQuestion2,
            securityAnswer2: AccountRegistrationStore.securityAnswer2,
        };

        this.state = {
            ...questionsAndAnswers,
            securityAnswerErrors: getErrors({...questionsAndAnswers}),
            securityQuestions: SecurityQuestionStore.securityQuestions,
            loading: true,
        };

        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    }

    componentDidMount() {
        AccountRegistrationStore.addChangeListener(this._updateState);
        SecurityQuestionStore.addChangeListener(this._populateSecurityQuestions);
        SecurityQuestionActions.loadSecurityQuestions();
    }

    componentWillUnmount() {
        SecurityQuestionStore.removeChangeListener(this._populateSecurityQuestions);
        AccountRegistrationStore.removeChangeListener(this._updateState);
    }

    _populateSecurityQuestions = () => {
        this.setState({securityQuestions: SecurityQuestionStore.securityQuestions, loading: false});
    }

    _updateState = () => {
        let {securityAnswer1, securityAnswer2, securityQuestion1, securityQuestion2} = AccountRegistrationStore;
        let questionsAndAnswers = {securityAnswer1, securityAnswer2, securityQuestion1, securityQuestion2};
        let securityAnswerErrors = getErrors(questionsAndAnswers);
        this.setState({...questionsAndAnswers, securityAnswerErrors});
    };

    _update = (value, inputName) => AccountRegistrationActions.updateRegistration({value, inputName});

    render() {
        if (this.state.loading) {
            return null;
        }
        return <div>
            <RegistrationHeader>
                <div className="text-center description-text">
                    Please select and answer the security questions below:
                </div>;
            </RegistrationHeader>
            <div className="row margin-top-40">
                <div className="col-xs-6 col-xs-offset-1">
                    {this._renderInputBoxes()}
                </div>
            </div>
        </div>;
    }

    _renderInputBoxes() {
        return <RegistrationBox>
            <SecurityQuestionSelector
                inputName='securityQuestion1'
                updateFunction={(value) => this._update(value, 'securityQuestion1')}
                securityQuestions={this.state.securityQuestions.filter(
                    (q) => q.id.toString() !== this.state.securityQuestion2
                )}
                value={this.state.securityQuestion1}
                placeholder='Select first security question...' />
            <RegistrationInput
                updateFunction={(value) => this._update(value, 'securityAnswer1')}
                placeholder="First Security Answer"
                value={this.state.securityAnswer1}
                name="security_answer_1"
                errors={[]} />
            <SecurityQuestionSelector
                inputName='securityQuestion2'
                updateFunction={(value) => this._update(value, 'securityQuestion2')}
                securityQuestions={this.state.securityQuestions.filter(
                    (q) => q.id.toString() !== this.state.securityQuestion1
                )}
                value={this.state.securityQuestion2}
                placeholder='Select second security question...' />
            <RegistrationInput
                updateFunction={(value) => this._update(value, 'securityAnswer2')}
                placeholder="Second Security Answer"
                value={this.state.securityAnswer2}
                name="security_answer_2"
                errors={this.state.securityAnswerErrors} />
            <div className="row">
                <button
                    disabled={this.state.securityAnswerErrors.length > 0}
                    onClick={() => this.props.goToScreen(AccountRegistrationScreens.REVIEW)}
                    className="next-btn col-md-8 col-sm-8 col-xs-8">Next</button>
            </div>
        </RegistrationBox>;
    }
}

SecurityQuestionScreen.propTypes = {
    goToScreen: PropTypes.func.isRequired,
};

const getErrors = ({securityAnswer1, securityAnswer2, securityQuestion1, securityQuestion2}) => {
    let errors = [];
    if (!securityQuestion1) {
        errors.push('First security question is required.');
    }
    if (!securityQuestion2) {
        errors.push('Second security question is required.');
    }
    if (!securityAnswer1 || securityAnswer1.length === 0) {
        errors.push('First security answer is required.');
    } else if (securityAnswer1.length > 50) {
        errors.push('First security answer cannot be longer than 50 characters.');
    }
    if (!securityAnswer2 || securityAnswer2.length === 0) {
        errors.push('Second security answer is required.');
    } else if (securityAnswer2.length > 50) {
        errors.push('Second security answer cannot be longer than 50 characters.');
    }
    if (securityAnswer1 === securityAnswer2) {
        errors.push('Security answers must not be the same.');
    }
    if (AccountRegistrationStore.invalidSecurityQuestion) {
        errors.push(AccountRegistrationErrors.invalidSecurityQuestion);
    }
    return errors;
};

const SecurityQuestionSelector = ({inputName, updateFunction, securityQuestions, value, placeholder}) => {
    return <div className="row" css={css({marginBottom: '20px'})}>
        <select
            css={css(selectStyles.container)}
            value={value}
            onChange={({target}) => updateFunction(target.value)}
            name={inputName}
            placeholder={placeholder}
        >
            <option css={css(selectStyles.placeholder)} value=''>{placeholder}</option>
            {securityQuestions.map(question => <option key={question.id}
                                                   css={css(selectStyles.option)}
                                                   value={question.id}>{question.question}</option>)}
        </select>
    </div>;
};

const selectStyles = {
    placeholder: { color: Colors.gray, fontSize: '14px', padding: '3px' },
    option: {
        fontSize: '14px',
        padding: '10px 16px',
        fontFamily: '"Gotham-Medium", Helvetica, Arial, sans-serif',
    },
    container: {
        border: '1px solid #ccc',
        position: 'relative',
        borderRadius: '4px',
        alignItems: 'center',
        display: 'flex',
        flex: '1 1 0%',
        flexWrap: 'wrap',
        padding: '2px 8px',
        overflow: 'hidden',
        boxSizing: 'border-box',
        height: '32px',
        width: '100%',
        fontSize: '14px',
    }
};

export default SecurityQuestionScreen;
