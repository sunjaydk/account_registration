/**
 * Created by Sunjay on February 24 2021
 */

'use strict';
/** @jsx jsx */

import {css, jsx} from '@emotion/core';
import PropTypes from 'prop-types';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import AccountRegistrationActions from '../../../actions/AccountRegistrationActions';
import PasswordRuleActions from '../../../actions/PasswordRuleActions';
import AccountRegistrationScreens from '../../../const/AccountRegistrationScreens';
import AccountRegistrationStore from '../../../stores/AccountRegistrationStore';
import PasswordRuleStore from '../../../stores/PasswordRuleStore';
import PasswordValidationUtil from '../../../utils/PasswordValidationUtil';
import LoadingText from '../LoadingText';
import RegistrationBox from '../RegistrationBox';
import RegistrationHeader from '../RegistrationHeader';
import RegistrationInput from '../RegistrationInput';

class PasswordScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: AccountRegistrationStore.password,
            confirmation: '',
            passwordRules: {},
            passwordErrors: [],
            confirmationErrors: [],
            isHidden: true,
            loading: true,
        };
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    }

    componentDidMount() {
        PasswordRuleStore.addChangeListener(this._populatePasswordRules);
        AccountRegistrationStore.addChangeListener(this._updateState);
        PasswordRuleActions.loadPasswordRules();
    }

    componentWillUnmount() {
        PasswordRuleStore.removeChangeListener(this._populatePasswordRules);
        AccountRegistrationStore.removeChangeListener(this._updateState);
    }

    _populatePasswordRules = () => {
        this.setState({
            passwordRules: PasswordRuleStore.passwordRules,
            passwordErrors: this._getPasswordErrors(AccountRegistrationStore.password),
            loading: false,
        });
    };

    _updateState = () => {
        this.setState({password: AccountRegistrationStore.password});
    };

    _getPasswordErrors = (password) => {
        if (Object.keys(this.state.passwordRules).length > 0) {
            return PasswordValidationUtil.getPasswordValidationErrors(password, this.state.passwordRules);
        } else {
            return PasswordValidationUtil.getDefaultPasswordValidationErrors(password);
        }
    };

    _updatePassword = (value) => {
        let {confirmation} = this.state;
        let confirmationErrors = checkConfirmation({password: value, confirmation});
        let passwordErrors = this._getPasswordErrors(value);
        this.setState({passwordErrors, confirmationErrors});
        AccountRegistrationActions.updateRegistration({value, inputName: 'password'});
    };

    _updateConfirmation = (value) => {
        let {password} = this.state;
        let confirmationErrors = checkConfirmation({password, confirmation: value});
        this.setState({confirmation: value, confirmationErrors});
    };

    render() {
        if (this.state.loading) {
            return <LoadingText/>;
        }
        return <div>
            <RegistrationHeader screenTitle='password'>
                {this._renderPasswordExample()}
            </RegistrationHeader>
            <div className="row margin-top-40">
                <div className="col-xs-6 col-xs-offset-1">
                    {this._renderInputBoxes()}
                </div>
            </div>
        </div>;
    }

    _renderPasswordExample() {
        if (Object.keys(this.state.passwordRules).length === 0) {
            return <div className="text-center description-text">
                <div>Your password must be between 6 and 15 characters long and contain both a letter and number.</div>
                <div>For example, <strong>'passw0rd'</strong> is acceptable, but 'w0rd' or 'password' is not.</div>
            </div>;
        }
        return null;
    }

    _renderInputBoxes() {
        return <RegistrationBox>
            <RegistrationInput
                updateFunction={((value) => this._updatePassword(value))}
                placeholder="Password"
                value={this.state.password}
                type={this.state.isHidden ? 'password' : 'text'}
                name="password"
                errors={this.state.passwordErrors} />
            <RegistrationInput
                updateFunction={((value) => this._updateConfirmation(value))}
                placeholder="Password confirmation"
                value={this.state.confirmation}
                type={this.state.isHidden ? 'password' : 'text'}
                name="password_confirmation"
                errors={this.state.confirmationErrors} />
            <label htmlFor='show_password'>
                <input type='checkbox'
                       id='show_password'
                       name='show_password'
                       checked={!this.state.isHidden}
                       onChange={() => this.setState({isHidden: !this.state.isHidden})}/>
                <span css={css({marginLeft: '.4em'})}>
                    Show password
                </span>
            </label>
            <div className="row">
                <button
                    disabled={this.state.passwordErrors.length > 0 || this.state.confirmationErrors.length > 0}
                    onClick={() => this.props.goToScreen(AccountRegistrationScreens.SECURITY_QUESTIONS)}
                    className="next-btn col-md-8 col-sm-8 col-xs-8">Next</button>
            </div>
        </RegistrationBox>;
    }
}

PasswordScreen.propTypes = {
    goToScreen: PropTypes.func.isRequired,
};

const checkConfirmation = ({password, confirmation}) => {
    let confirmationErrors = [];
    if (password !== confirmation) {
        confirmationErrors.push('Passwords do not match.');
    }
    return confirmationErrors;
};

export default PasswordScreen;
