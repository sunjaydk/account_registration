/**
 * Created by Sunjay on February 11 2021
 */

'use strict';

import PropTypes from 'prop-types';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import AccountRegistrationActions from '../../../actions/AccountRegistrationActions';
import AccountRegistrationScreens from '../../../const/AccountRegistrationScreens';
import AccountRegistrationStore from '../../../stores/AccountRegistrationStore';
import RegistrationBox from '../RegistrationBox';
import RegistrationHeader from '../RegistrationHeader';
import RegistrationInput from '../RegistrationInput';

class NameScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: AccountRegistrationStore.firstName,
            lastName: AccountRegistrationStore.lastName,
            firstNameErrors: getNameErrors(AccountRegistrationStore.firstName),
            lastNameErrors: getNameErrors(AccountRegistrationStore.lastName),
        };
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    }

    componentDidMount() {
        AccountRegistrationStore.addChangeListener(this._updateState);
    }

    componentWillUnmount() {
        AccountRegistrationStore.removeChangeListener(this._updateState);
    }

    _updateState = () => {
        let {firstName, lastName} = AccountRegistrationStore;
        this.setState({
            firstName, lastName,
            firstNameErrors: getNameErrors(firstName),
            lastNameErrors: getNameErrors(lastName),
        });
    };

    _update = (value, inputName) => AccountRegistrationActions.updateRegistration({value, inputName});

    render() {
        return <div>
            <RegistrationHeader>
                <div className="text-center description-text">
                    Please enter your <strong>full legal name</strong> below:
                </div>;
            </RegistrationHeader>
            <div className="row margin-top-40">
                <div className="col-xs-6 col-xs-offset-1">
                    {this._renderInputBoxes()}
                </div>
            </div>
        </div>;
    }

    _renderInputBoxes() {
        return <RegistrationBox>
            <RegistrationInput
               updateFunction={((value) => this._update(value, 'firstName'))}
               placeholder="Legal First Name"
               value={this.state.firstName}
               name="first_name"
               errors={this.state.firstNameErrors} />
            <RegistrationInput
               updateFunction={((value) => this._update(value, 'lastName'))}
               placeholder="Legal Last Name"
               value={this.state.lastName}
               name="last_name"
               errors={this.state.lastNameErrors} />
            <div className="row">
                <button
                    disabled={this.state.firstNameErrors.length > 0 || this.state.lastNameErrors.length > 0}
                    onClick={() => this.props.goToScreen(AccountRegistrationScreens.INMATE_ID)}
                    className="next-btn col-md-8 col-sm-8 col-xs-8">Next</button>
            </div>
        </RegistrationBox>;
    }
}

NameScreen.propTypes = {
    goToScreen: PropTypes.func.isRequired,
};

const getNameErrors = (name) => {
    let nameErrors = [];
    if (name.length === 0) {
        nameErrors.push('Name is required.');
    } else if (name.length > 50) {
        nameErrors.push('Name cannot be longer than 50 characters.');
    }
    if (/[^a-zA-Z-'\s]/.test(name)) {
        nameErrors.push('Only letters, spaces, apostrophes (\') and hyphens (-) are allowed.');
    }
    return nameErrors;
};

export default NameScreen;
