/**
 * Created by Sunjay on February 24 2021
 */

'use strict';
/** @jsx jsx */

import {css, jsx} from '@emotion/core';
import PropTypes from 'prop-types';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import AccountRegistrationActions from '../../../actions/AccountRegistrationActions';
import AccountRegistrationErrors from '../../../const/AccountRegistrationErrors';
import AccountRegistrationScreens from '../../../const/AccountRegistrationScreens';
import Colors from '../../../const/Colors';
import AccountRegistrationStore from '../../../stores/AccountRegistrationStore';
import {getHTMLDateString} from '../../../utils/DateUtil';
import RegistrationBox from '../RegistrationBox';
import RegistrationError from '../RegistrationError';
import RegistrationHeader from '../RegistrationHeader';

class DateOfBirthScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dateOfBirth: AccountRegistrationStore.dateOfBirth || DEFAULT_BIRTH_DATE,
            disabled: !AccountRegistrationStore.dateOfBirth || AccountRegistrationStore.invalidDOB,
            invalidDOB: AccountRegistrationStore.invalidDOB,
        };
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    }

    componentDidMount() {
        AccountRegistrationStore.addChangeListener(this._updateState);
    }

    componentWillUnmount() {
        AccountRegistrationStore.removeChangeListener(this._updateState);
    }

    _updateState = () => {
        this.setState({dateOfBirth: AccountRegistrationStore.dateOfBirth, disabled: false, invalidDOB: AccountRegistrationStore.invalidDOB});
    };

    _update = (value) => AccountRegistrationActions.updateRegistration({value, inputName: 'dateOfBirth'});

    render() {
        return <div>
            <RegistrationHeader>
                <div className="text-center description-text">
                    Please enter your date of birth below:
                </div>
            </RegistrationHeader>
            <div className="row margin-top-40">
                <div className="col-xs-6 col-xs-offset-1">
                    {this._renderInput()}
                </div>
            </div>
        </div>;
    }

    _renderInput() {
        return <RegistrationBox>
            <div className="row" css={css({marginBottom: '20px'})}>
                <div className="fg-line">
                    <span className="float-label">
                        <input type="date"
                               onChange={(({target}) => this._update(target.value))}
                               className="keyboard form-control input-lg no-shadow"
                               placeholder={DEFAULT_BIRTH_DATE}
                               name='date_of_birth'
                               id='date_of_birth'
                               css={this.state.disabled ? css({color: Colors.gray}) : css({color: Colors.black})}
                               min={MIN_BIRTH_DATE}
                               max={MAX_BIRTH_DATE}
                               value={this.state.dateOfBirth}/>
                        <label htmlFor='date_of_birth'>Date of Birth</label>
                    </span>
                </div>
            </div>
            {this.state.invalidDOB ? <RegistrationError error={AccountRegistrationErrors.invalidDOB}/> : null}
            <div className="row">
                <button
                    disabled={this.state.disabled}
                    onClick={() => this.props.goToScreen(AccountRegistrationScreens.PASSWORD)}
                    className="next-btn col-md-8 col-sm-8 col-xs-8">Next</button>
            </div>
        </RegistrationBox>;
    }
}

DateOfBirthScreen.propTypes = {
    goToScreen: PropTypes.func.isRequired,
};

const DEFAULT_BIRTH_DATE = getHTMLDateString(new Date().setFullYear(new Date().getFullYear() - 30));

const MAX_BIRTH_DATE = getHTMLDateString(new Date().setFullYear(new Date().getFullYear() - 10));

const MIN_BIRTH_DATE = getHTMLDateString(new Date().setFullYear(new Date().getFullYear() - 100));

export default DateOfBirthScreen;
