/**
 * Created by Sunjay on February 23 2021
 */

'use strict';
/** @jsx jsx */

import {css, jsx} from '@emotion/core';
import Colors from '../../const/Colors';

const RegistrationError = ({error}) => <div key={error} css={css({color: Colors.darkRed})}>{error}</div>;

export default RegistrationError;
