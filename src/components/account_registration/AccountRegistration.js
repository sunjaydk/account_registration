/**
 * Created by Sunjay on December 17, 2020
 */

'use strict';
/** @jsx jsx */

import {css, jsx} from '@emotion/core';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import AccountRegistrationActions from '../../actions/AccountRegistrationActions';
import AccountRegistrationScreens from '../../const/AccountRegistrationScreens';
import Colors from '../../const/Colors';
import FacilityRuleStore from '../../stores/FacilityRuleStore';
import Toastr from '../../utils/Toastr';
import DateOfBirthScreen from './screens/DateOfBirthScreen';
import InmateIdScreen from './screens/InmateIdScreen';
import NameScreen from './screens/NameScreen';
import PasswordScreen from './screens/PasswordScreen';
import ReviewScreen from './screens/ReviewScreen';
import SecurityQuestionScreen from './screens/SecurityQuestionScreen';
import LoadingText from './LoadingText';
import ProgressBar from './ProgressBar';

class AccountRegistration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentScreen: null,
        };
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    }

    componentDidMount() {
        AccountRegistrationActions.init();
        FacilityRuleStore.addChangeListener(this._updateState);
    }

    componentWillUnmount() {
        FacilityRuleStore.removeChangeListener(this._updateState);
    }

    _updateState = () => {
        if (!this.state.currentScreen) {
            this.setState({
                useIDFeed: FacilityRuleStore.useIDFeed,
                currentScreen: FacilityRuleStore.useIDFeed ?
                    AccountRegistrationScreens.INMATE_ID :
                    AccountRegistrationScreens.NAME,
            });
        }
    };

    _goToScreen = (screen) => this.setState({currentScreen: screen})

    _goBack = () => {
        if (this.state.currentScreen === AccountRegistrationScreens.NAME ||
            (this.state.currentScreen === AccountRegistrationScreens.INMATE_ID && FacilityRuleStore.useIDFeed)) {
            // Temporary use case - in the log run we will Reactify the login page as well and use browsereHistory.push
            window.location.replace('/login');
        } else {
            switch (this.state.currentScreen) {
                case AccountRegistrationScreens.INMATE_ID:
                    this._goToScreen(AccountRegistrationScreens.NAME);
                    break;
                case AccountRegistrationScreens.DATE_OF_BIRTH:
                    this._goToScreen(AccountRegistrationScreens.INMATE_ID);
                    break;
                case AccountRegistrationScreens.PASSWORD:
                    this._goToScreen(AccountRegistrationScreens.DATE_OF_BIRTH);
                    break;
                case AccountRegistrationScreens.SECURITY_QUESTIONS:
                    this._goToScreen(AccountRegistrationScreens.PASSWORD);
                    break;
                case AccountRegistrationScreens.REVIEW:
                    this._goToScreen(AccountRegistrationScreens.SECURITY_QUESTIONS);
                    break;
                default:
                    Toastr.error('Invalid screen selected!');
            }
        }
    }

    render() {
        return <div>
            <div css={css(blueBgStyles)} />
            <div className="container-fluid padding-left-15 padding-right-15">
                <div className="row">
                    <div onClick={this._goBack} className="col-md-2 col-sm-2 col-xs-2" css={css(backBtnStyles)}>
                        <span className="i-arrow-l"/>Back
                    </div>
                    <div className="col-md-4 col-sm-4 col-xs-4"/>
                    <div className="col-md-2 col-sm-2 col-xs-2">
                        <img align="right" height="60" src="/images/restyle/logo-short.png"/>
                    </div>
                </div>
                {this._renderContainer()}
            </div>
        </div>;
    }

    _renderContainer() {
        if (!this.state.currentScreen) {
            return <LoadingText />;
        }
        return <div className='container'>
            <ProgressBar
                goToScreen={this._goToScreen}
                currentScreen={this.state.currentScreen}/>
            {this._renderScreen()}
        </div>;
    }

    _renderScreen() {
        switch (this.state.currentScreen) {
            case AccountRegistrationScreens.NAME:
                return <NameScreen goToScreen={this._goToScreen} />;
            case AccountRegistrationScreens.INMATE_ID:
                return <InmateIdScreen goToScreen={this._goToScreen} />;
            case AccountRegistrationScreens.DATE_OF_BIRTH:
                return <DateOfBirthScreen goToScreen={this._goToScreen} />;
            case AccountRegistrationScreens.PASSWORD:
                return <PasswordScreen goToScreen={this._goToScreen} />;
            case AccountRegistrationScreens.SECURITY_QUESTIONS:
                return <SecurityQuestionScreen goToScreen={this._goToScreen} />;
            case AccountRegistrationScreens.REVIEW:
                return <ReviewScreen goToScreen={this._goToScreen} />;
            default:
                return null;
        }
    }
}

const blueBgStyles = {
    position: 'absolute',
    left: '0',
    right: '0',
    top: '0',
    zIndex: '-1',
    height: '69vh',
    minHeight: '470px',
    background: 'url(/images/login-bg.png) repeat center top',
};

const backBtnStyles = {
    color: Colors.blueWhite,
    fontSize: '18px',
    position: 'relative',
    padding: '7px  0px 0px 22px',
    textAlign: 'left',
    backgroundColor: 'transparent',
    borderWidth: '0px',
};

export default AccountRegistration;
