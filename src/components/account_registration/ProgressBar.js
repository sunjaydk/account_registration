/**
 * Created by Sunjay on February 17, 2021
 */

'use strict';
/** @jsx jsx */

import {css, jsx} from '@emotion/core';
import PropTypes from 'prop-types';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import AccountRegistrationScreens from '../../const/AccountRegistrationScreens';
import Colors from '../../const/Colors';
import AccountRegistrationStore from '../../stores/AccountRegistrationStore';
import FacilityRuleStore from '../../stores/FacilityRuleStore';

class ProgressBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: AccountRegistrationStore.firstName,
            lastName: AccountRegistrationStore.lastName,
            inmateId: AccountRegistrationStore.inmateId,
            dateOfBirth: AccountRegistrationStore.dateOfBirth,
            password: AccountRegistrationStore.password,
            securityQuestion1: AccountRegistrationStore.securityQuestion1,
            securityAnswer1: AccountRegistrationStore.securityAnswer1,
            securityQuestion2: AccountRegistrationStore.securityQuestion2,
            securityAnswer2: AccountRegistrationStore.securityAnswer2,
        };
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
    }

    componentDidMount() {
        AccountRegistrationStore.addChangeListener(this._updateRegistrationState);
    }

    componentWillUnmount() {
        AccountRegistrationStore.removeChangeListener(this._updateRegistrationState);
    }

    _updateRegistrationState = () => {
        this.setState({
            firstName: AccountRegistrationStore.firstName,
            lastName: AccountRegistrationStore.lastName,
            inmateId: AccountRegistrationStore.inmateId,
            dateOfBirth: AccountRegistrationStore.dateOfBirth,
            password: AccountRegistrationStore.password,
            securityQuestion1: AccountRegistrationStore.securityQuestion1,
            securityAnswer1: AccountRegistrationStore.securityAnswer1,
            securityQuestion2: AccountRegistrationStore.securityQuestion2,
            securityAnswer2: AccountRegistrationStore.securityAnswer2,
        });
    };

    render() {
        return <div className="row">
            <div className="col-md-1 col-sm-1"/>
            <div className="col-md-6 col-sm-6">
                <ul css={css(barStyles)}>{this._renderBreadcrumbs()}</ul>
            </div>
        </div>;
    }

    _renderBreadcrumbs = () => {
        if (FacilityRuleStore.useIDFeed) {
            return Object.keys(AccountRegistrationScreens).slice(1).map(this._renderBreadcrumb);
        } else {
            return Object.keys(AccountRegistrationScreens).map(this._renderBreadcrumb);
        }
    };

    _renderBreadcrumb = (screen, index) => {
        if (this.props.currentScreen === screen) {
            return <li key={screen} data-testid={screen} css={css(currentBreadcrumbStyles)}>
                <span css={css(currentBubbleStyles)}>{index + 1}</span>
                {ScreenTitle[screen]}
                {index > 0 ? <span css={css(currentOrCompletedStemStyles)}/> : null}
            </li>;
        } else if (this._isComplete(screen)) {
            return <li key={screen}
                       onClick={() => this.props.goToScreen(screen)}
                       css={css(completedBreadcrumbStyles)}>
                <span css={css(completedBubbleStyles)}>&#10003;</span>
                {ScreenTitle[screen]}
                {index > 0 ? <span css={css(currentOrCompletedStemStyles)}/> : null}
            </li>;
        } else {
            return <li key={screen}
                       data-testid={screen}
                       onClick={this._canJumpTo(screen) ? () => this.props.goToScreen(screen) : () => {}}
                       css={css(futureBreadcrumbStyles)}>
                <span css={css(futureBubbleStyles)}>{index + 1}</span>
                {ScreenTitle[screen]}
                {index > 0 ? <span css={css(futureStemStyles)}/> : null}
            </li>;
        }
    }

    _canJumpTo = (screen) => {
        switch (screen) {
            case AccountRegistrationScreens.NAME:
                return true;
            case AccountRegistrationScreens.INMATE_ID:
                return (this.state.firstName.length > 0 && this.state.lastName.length > 0) || Boolean(FacilityRuleStore.useIDFeed);
            case AccountRegistrationScreens.DATE_OF_BIRTH:
                return this.state.inmateId.length > 0;
            case AccountRegistrationScreens.PASSWORD:
                return Boolean(this.state.dateOfBirth);
            case AccountRegistrationScreens.SECURITY_QUESTIONS:
                return this.state.password.length > 0;
            case AccountRegistrationScreens.REVIEW:
                return this.state.securityQuestion1.length > 0 && this.state.securityAnswer1.length > 0 &&
                    this.state.securityQuestion2.length > 0 && this.state.securityAnswer2.length > 0;
            default:
                return false;
        }
    };

    _isComplete = (screen) => {
        switch (screen) {
            case AccountRegistrationScreens.NAME:
                return (this.state.firstName.length > 0 && this.state.lastName.length > 0) || Boolean(FacilityRuleStore.useIDFeed);
            case AccountRegistrationScreens.INMATE_ID:
                return this.state.inmateId.length > 0;
            case AccountRegistrationScreens.DATE_OF_BIRTH:
                return Boolean(this.state.dateOfBirth);
            case AccountRegistrationScreens.PASSWORD:
                return this.state.password.length > 0;
            case AccountRegistrationScreens.SECURITY_QUESTIONS:
                return this.state.securityQuestion1.length > 0 && this.state.securityAnswer1.length > 0 &&
                    this.state.securityQuestion2.length > 0 && this.state.securityAnswer2.length > 0;
            case AccountRegistrationScreens.REVIEW:
            default:
                return false;
        }
    };
}

ProgressBar.propTypes = {
    currentScreen: PropTypes.string.isRequired,
    goToScreen: PropTypes.func.isRequired,
};

const ScreenTitle = {
    [AccountRegistrationScreens.NAME]: 'Name',
    [AccountRegistrationScreens.INMATE_ID]: FacilityRuleStore.inmateIdName,
    [AccountRegistrationScreens.DATE_OF_BIRTH]: 'Date of Birth',
    [AccountRegistrationScreens.PASSWORD]: 'Password',
    [AccountRegistrationScreens.SECURITY_QUESTIONS]: 'Security Questions',
    [AccountRegistrationScreens.REVIEW]: 'Review',
};

const barStyles = {
    display: 'flex',
    marginBottom: '20px',
};

const breadcrumbStyles = {
    listStyleType: 'none',
    width: '25%',
    position: 'relative',
    textAlign: 'center',
    textTransform: 'uppercase',
};

const futureBreadcrumbStyles = {
    ...breadcrumbStyles,
    color: Colors.blueWhite,
    fontSize: '10px',
};

const currentBreadcrumbStyles = {
    ...breadcrumbStyles,
    color: Colors.white,
    fontWeight: 'bolder',
    fontSize: '13px',
};

const completedBreadcrumbStyles = {
    ...breadcrumbStyles,
    color: Colors.white,
    fontSize: '10px',
};

const bubbleStyles = {
    borderRadius: '50%',
    display: 'block',
    textAlign: 'center',
    boxShadow: '0px 3px 15px rgba(0,0,0,0.2)',
};

const currentBubbleStyles = {
    ...bubbleStyles,
    color: Colors.black,
    backgroundColor: Colors.white,
    width: '40px',
    height: '40px',
    lineHeight: '40px',
    margin: '-5px auto 10px auto',
};

const completedBubbleStyles = {
    ...bubbleStyles,
    width: '30px',
    height: '30px',
    lineHeight: '30px',
    color: Colors.green,
    backgroundColor: Colors.white,
    border: '2px solid' + Colors.green,
    margin: '0 auto 10px auto',
};

const futureBubbleStyles = {
    ...bubbleStyles,
    width: '30px',
    height: '30px',
    lineHeight: '30px',
    backgroundColor: Colors.gray,
    color: Colors.black,
    margin: '0 auto 10px auto',
};

const stemStyles = {
    width: '100%',
    height: '5px',
    position: 'absolute',
    top: '12px',
    left: '-50%',
    zIndex: '-1',
    boxShadow: '0px 3px 15px rgba(0,0,0,0.2)',
};

const futureStemStyles = {
    ...stemStyles,
    backgroundColor: Colors.gray,
};

const currentOrCompletedStemStyles = {
    ...stemStyles,
    backgroundColor: Colors.white,
    border: '0.5px solid ' + Colors.green,
};

export default ProgressBar;
