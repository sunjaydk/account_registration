/**
 * Created by Sunjay on February 24 2021
 */

'use strict';
/** @jsx jsx */

import {css, jsx} from '@emotion/core';
import RegistrationError from './RegistrationError';

const RegistrationInput = ({updateFunction, placeholder, value, name, errors, type = 'text'}) => {
    return <div className="row" css={css({marginBottom: '20px'})}>
        <div className="fg-line">
            <span className="float-label">
                <input type={type}
                       onChange={(({target}) => updateFunction(target.value))}
                       className="keyboard form-control input-lg no-shadow"
                       placeholder={placeholder}
                       name={name}
                       id={name}
                       value={value}/>
                <label htmlFor={name}>{placeholder}</label>
            </span>
        </div>
        <div className='w-inpt rt'>
            {errors.map(error => <RegistrationError error={error} key={error} />)}
        </div>
    </div>;
};

export default RegistrationInput;
