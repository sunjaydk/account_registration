/**
 * Created by Sunjay on Feburary 24, 2021
 */

'use strict';
/** @jsx jsx */

import {css, jsx} from '@emotion/core';
import Colors from '../../const/Colors';

const LoadingText = () => <h1 css={css(loadingStyles)}>Loading...</h1>;

const loadingStyles = {
    margin: '100px auto',
    textAlign: 'center',
    color: Colors.white,
};

export default LoadingText;
