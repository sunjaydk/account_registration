/**
 * Created by Sunjay on February 23 2021
 */

'use strict';
/** @jsx jsx */

import {css, jsx} from '@emotion/core';
import Colors from '../../const/Colors';

const RegistrationHeader = ({children}) => {
    return <div css={css({color: Colors.white})} className="row">
        <div className="text-center margin-top-30 margin-bottom-20 welcome-text">
            Welcome to Edovo!
        </div>
        {children}
    </div>;
};

export default RegistrationHeader;
