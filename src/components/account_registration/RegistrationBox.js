/**
 * Created by Sunjay on February 23 2021
 */

'use strict';
/** @jsx jsx */

import {css, jsx} from '@emotion/core';
import Colors from '../../const/Colors';

const RegistrationBox = ({children}) => <div css={css(boxStyles)}>{children}</div>;

export default RegistrationBox;

const boxStyles = {
    padding: '25px',
    backgroundColor: Colors.white,
    boxShadow: '0 8px 16px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%)',
    marginBottom: '20px',
};
